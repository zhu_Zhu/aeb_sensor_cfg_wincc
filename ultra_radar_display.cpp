﻿// ultra_radar_display.cpp: 实现文件
//

#include "pch.h"
#include "Sensor_Param_Hex.h"
#include "ultra_radar_display.h"
#include "afxdialogex.h"


// ultra_radar_display 对话框

IMPLEMENT_DYNAMIC(ultra_radar_display, CDialogEx)

ultra_radar_display::ultra_radar_display(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_ULTRAL_DIS, pParent)
{

}

ultra_radar_display::~ultra_radar_display()
{
}

void ultra_radar_display::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(ultra_radar_display, CDialogEx)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// ultra_radar_display 消息处理程序


BOOL ultra_radar_display::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	LoadPicture();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}

void ultra_radar_display::LoadPicture(void)//形参可以为所给的图片对象的指针，这里为了通用性省去了形参
{
	image.Load(_T("Ultra_Radar.png")); //根据图片路径加载图片 

	//int cx = image.GetWidth();//获取图片宽度 
	//int cy = image.GetHeight();//获取图片高度 
	//printf("picture:width>%d\thight>%d\n", cx, cy);

	if (image.IsNull())    //判断有无图像  
		return;

	CWnd* pWnd = GetDlgItem(IDC_STA_ULTRA_PIC);//获得pictrue控件窗口的句柄 
	pWnd->GetClientRect(&rect);//获得pictrue控件所在的矩形区域 

	SetTimer(2, 50, NULL);
}


void ultra_radar_display::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialogEx::OnTimer(nIDEvent);
	switch (nIDEvent) {
	case 2: {
		CDC* pDC = GetDlgItem(IDC_STA_ULTRA_PIC)->GetDC();//获得pictrue控件的DC  
		image.Draw(pDC->m_hDC, rect); //将图片画到Picture控件表示的矩形区域
		ReleaseDC(pDC);//释放picture控件的DC
	}break;
	}
}
