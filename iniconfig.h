#pragma once
#ifndef _INICONFIG_H

#define _INICONFIG_H
#include <Windows.h>
#include <stdio.h>

class CIniConfig
{
public:
#if 0
    CIniConfig(char* file)
    {
        strcpy(file_name, file);
    }
#else
    CIniConfig() {
        memset(file_name, 0, sizeof(file_name));
    }

    void SetIniFilePath(char* file)
    {
        strcpy(file_name, file);
    }
#endif
    // getter
    unsigned GetNumber(char* section_name, char* key_name)
    {
        return GetPrivateProfileInt(section_name, key_name, 0, file_name);
    }

    // setter
    void SetNumber(char* section_name, char* key_name, unsigned val)
    {
        char buf[100] = { 0 };
        sprintf(buf, "%d", val);
        WritePrivateProfileString(section_name, key_name, buf, file_name);
    }

    void GetString(char* section_name, char* key_name, char* out_buf)
    {
        GetPrivateProfileString(section_name, key_name, "", out_buf, 100, file_name);
    }

    void SetString(char* section_name, char* key_name, char* in_buf)
    {
        WritePrivateProfileString(section_name, key_name, in_buf, file_name);
    }

private:
    char file_name[MAX_PATH];

};

#endif  // _INICONFIG_H

