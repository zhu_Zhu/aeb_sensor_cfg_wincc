﻿
// Sensor_Param_HexDlg.h: 头文件
//

#pragma once
#include "ultra_radar_display.h"
#include "mmw_radar_display.h"
#include "COwerButton.h"
#include "iniconfig.h"

#define DEFAULT_COLOUR		RGB(240, 240, 240)
#define DISABLE_COLOUR		RGB(255, 0, 0)
#define ENABLE_COLOUR		RGB(0, 255, 0)
#define MY_FRONT_SIZE		16

/******************** hex 存储时参数配置结构体 ******************** begin ********/
/* 传感器配置（公用）*/
typedef struct _Sensor_HEX_Com_CFG
{
	volatile uint8_t m_isOpen;			// 是否开启
	uint8_t m_producer;					// 厂家
//	uint8_t	m_sensor_num;				// 数目
//	uint8_t	m_scene;					// 场景
}Sensor_Hex_Com_CFG;

/* 传感器位置  */
typedef struct _Sensor_Utral_Site_Info
{
	volatile uint8_t m_main_sw;			// 单排控制开关	m_main_sw

	uint8_t m_controler;				// 控制器编号，从1开始取值
	uint8_t m_producer;					// 厂家
	uint8_t m_sensor_num;				// 传感器数据
	// 车头行进方向为正， 车前方从左到右编号依次1~6
	// 车头行进方向为正，车后方从左到右编号依次1~6
	// 车头行进方向为正，车左方从车头到车尾编号依次1~6
	// 车头行进方向为正，车右方从车头到车尾编号依次1~6

	// 每个雷达开关，其中暂定3号和4号就是车牌左右位置；
	uint8_t m_rdr_sw;	// 第5bit表示6号；第4bit表示5号；第3bit表示4号；第2bit表示3号；第1bit表示2号；第0bit表示1号；
}Sensor_Utral_Site_Info;

/* 传感器位置  */
typedef struct _Sensor_MMW_Site_Info
{
	volatile uint8_t m_main_sw;			// 单排控制开关

	uint8_t m_sensor_num;				// 传感器数据
	uint8_t m_producer;					// 厂家
	// 前后默认1个雷达就是1号雷达，左右两侧默认2个
	// 车头行进方向为正，车左方从车头到车尾编号依次1~2
	// 车头行进方向为正，车右方从车头到车尾编号依次1~2
	uint8_t m_rdr_sw;
}Sensor_MMW_Site_Info;

/* 超声波雷达配置 */
typedef struct _Sensor_UtraRdr_CFG
{
	Sensor_Utral_Site_Info m_front;			// 默认8个（2+4+2）
	Sensor_Utral_Site_Info m_end;			// 默认8个（2+4+2）
	Sensor_Utral_Site_Info m_left;			// 默认4个
	Sensor_Utral_Site_Info m_right;			// 默认4个
}Sensor_UtraRdr_CFG;

/* 毫米波雷达配置  */
typedef struct _Sensor_MMWRdr_CFG
{
	//	volatile uint8_t m_isOpen;				// 总控制开关

	Sensor_MMW_Site_Info m_front;			// 默认1个
	Sensor_MMW_Site_Info m_right;			// 默认1个
	Sensor_MMW_Site_Info m_end;				// 默认0个
	Sensor_MMW_Site_Info m_left;			// 默认0个
}Sensor_MMWRdr_CFG;

/* 传感器管理（最终结构体）  */
typedef struct _Sensor_Hex_MGT
{
	Sensor_Hex_Com_CFG 		m_4g;			// 4G功能
	Sensor_Hex_Com_CFG		m_gps;			// GPS功能
	Sensor_Hex_Com_CFG 		m_dbl_cam;		// 双面相机
	Sensor_Hex_Com_CFG 		m_sgl_cam;		// 单目相机
	Sensor_UtraRdr_CFG 	m_ultra_radar;	// 超声波雷达
	Sensor_MMWRdr_CFG 	m_mmw_radar;	// 毫米波雷达
	Sensor_Hex_Com_CFG 		m_imu;			// IMU惯性测量仪
	Sensor_Hex_Com_CFG 		m_car_dvr;		// 行车记录仪
	Sensor_Hex_Com_CFG		m_actuator;		// 执行机构（比例阀，EBS电子制动系统）

	uint8_t				m_project;		// 描述项目类型
	uint8_t 			m_product;		// 描述产品类型
	// 预留
//	Sensor_Com_CFG m_spd_ecder;			// 车速编码器
//	Sensor_Com_CFG m_infra_cam;			// 红外相机
//	Sensor_Com_CFG m_blind_detect_cam;	// 盲区检测相机
}Sensor_Hex_MGT;



/******************* hex 存储时参数配置结构体 ***************** end ***********/





typedef struct _Project_Product_CFG
{
	//char m_type[32][20];		// 类型
	char m_use_type[20];		// 正使用的类型
	char m_type[6][20];		// 类型
	int m_type_num;				// 类型个数
	int m_type_index;			// 在使用类型的下标，从0开始计数
}Project_Product_CFG;


typedef struct _Sensor_Com_CFG
{
	bool m_func_sw;					// 是否开启
	//char m_producer[16][20];		// 厂家
	char m_use_prouducer[20];		// 正使用的厂家
	char m_producer[6][20];		// 厂家
	int m_producer_num;				// 厂家个数
	int m_producer_index;			// 在使用厂家的下标，从0开始计数
}Sensor_Com_CFG;

/* 超声波雷达配置参数  */
typedef struct _Sensor_Utral_CFG
{
	bool m_func_sw;					// 单排控制开关	TRUE

	int m_controler;				// 控制器编号，从1开始取值
	char m_use_prouducer[20];		// 正使用的厂家
	char m_producer[8][20];			// 厂家
	int m_producer_num;				// 厂家个数
	int m_producer_index;			// 在使用厂家的下标，从0开始计数
	int controler_id;				// 控制器编号		
	int m_sensor_num;				// 传感器个数
	// 车头行进方向为正， 车前方从左到右编号依次1~6
	// 车头行进方向为正，车后方从左到右编号依次1~6
	// 车头行进方向为正，车左方从车头到车尾编号依次1~6
	// 车头行进方向为正，车右方从车头到车尾编号依次1~6

	// 每个雷达开关，其中暂定3号和4号就是车牌左右位置；
	bool m_rdr1_sw;					// 1号雷达开关
	bool m_rdr2_sw;
	bool m_rdr3_sw;
	bool m_rdr4_sw;
	bool m_rdr5_sw;
	bool m_rdr6_sw;
}Sensor_Utral_CFG;

/* 毫米波雷达配置参数 */
typedef struct _Sensor_MMW_CFG
{
	bool m_func_sw;					// 单排控制开关

	//uint8_t m_sensor_num;			// 传感器数据
	char m_use_prouducer[20];		// 正使用的厂家
	char m_producer[8][20];			// 厂家
	int m_producer_num;				// 厂家个数
	int m_producer_index;			// 在使用厂家的下标，从0开始计数
	int m_sensor_num;				// 传感器个数
	// 前后默认1个雷达就是1号雷达，左右两侧默认2个
	// 车头行进方向为正，车左方从车头到车尾编号依次1~2
	// 车头行进方向为正，车右方从车头到车尾编号依次1~2
	bool m_rdr1_sw;					// 1号雷达开关
	bool m_rdr2_sw;
}Sensor_MMW_CFG;


typedef struct _Config_Param
{
	Project_Product_CFG m_project_type;	// 项目类型
	Project_Product_CFG m_product_type;	// 产品类型

	Sensor_Com_CFG m_4g;
	Sensor_Com_CFG m_gps;
	Sensor_Com_CFG m_imu;
	Sensor_Com_CFG m_dbl_cam;
	Sensor_Com_CFG m_sgl_cam;
	Sensor_Com_CFG m_car_dvr;
	Sensor_Com_CFG m_brk_act;

	Sensor_Utral_CFG m_ultra_front;
	Sensor_Utral_CFG m_ultra_end;
	Sensor_Utral_CFG m_ultra_left;
	Sensor_Utral_CFG m_ultra_right;

	Sensor_MMW_CFG m_mmw_front;
	Sensor_MMW_CFG m_mmw_end;
	Sensor_MMW_CFG m_mmw_left;
	Sensor_MMW_CFG m_mmw_right;
}Config_Param;

typedef struct _Common_Config
{
	volatile bool m_func_btn_sw;	// 功能按钮状态
	int m_producer;					// 厂家

	CMFCButton m_func_btn_handle;	// 功能按钮句柄
	CComboBox m_prodcr_comb_handle;	// 控件combox-box厂家句柄
}Common_Config;

typedef struct _Ultra_Config
{
	bool m_func_btn_sw;				// 功能按钮状态
	int m_producer;					// 厂家
	bool m_rdr1_btn_sw;					// 1号雷达按钮状态
	bool m_rdr2_btn_sw;					// 2号雷达按钮状态
	bool m_rdr3_btn_sw;					// 3号雷达按钮状态
	bool m_rdr4_btn_sw;					// 4号雷达按钮状态
	bool m_rdr5_btn_sw;					// 5号雷达按钮状态
	bool m_rdr6_btn_sw;					// 6号雷达按钮状态

	CMFCButton m_func_btn_handle;	// 功能按钮句柄
	CMFCButton m_rdr1_btn_handle;	// 1号按钮句柄
	CMFCButton m_rdr2_btn_handle;	// 2号按钮句柄
	CMFCButton m_rdr3_btn_handle;	// 3号按钮句柄
	CMFCButton m_rdr4_btn_handle;	// 4号按钮句柄
	CMFCButton m_rdr5_btn_handle;	// 5号按钮句柄
	CMFCButton m_rdr6_btn_handle;	// 6号按钮句柄
	CComboBox m_prodcr_comb_handle;	// 控件combox-box厂家句柄
	CComboBox m_ctrler_comb_handle;	// 控件combox-box控制器编号

	volatile int rdr_num;					// 雷达个数
}Ultra_Config;

typedef struct _MMW_Config
{
	bool m_func_btn_sw;				// 功能按钮状态
	int m_producer;					// 厂家
	bool m_rdr1_btn_sw;					// 1号雷达按钮状态
	bool m_rdr2_btn_sw;					// 1号雷达按钮状态
	int rdr_num;					// 雷达数量

	CMFCButton m_func_btn_handle;	// 功能按钮句柄
	CMFCButton m_rdr1_btn_handle;	// 1号按钮句柄
	CMFCButton m_rdr2_btn_handle;	// 2号按钮句柄
	CComboBox m_prodcr_comb_handle;	// 控件combox-box厂家句柄
}MMW_Config;


// CSensorParamHexDlg 对话框
class CSensorParamHexDlg : public CDialogEx
{
// 构造
public:
	CSensorParamHexDlg(CWnd* pParent = nullptr);	// 标准构造函数
	~CSensorParamHexDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SENSOR_PARAM_HEX_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	CImage image;		// 创建图片类 
	CRect rect;			// 定义矩形类 
	CFont* l_front;		// 字体
	bool m_ultra_dis_btn_sw;
	bool m_mmw_dis_btn_sw;
	CIniConfig m_iniFile_handle;	// ini文件句柄
	char m_iniFile_path[MAX_PATH];	// ini配置文件路径

	CIniConfig m_hexFile_handle;	// hex文件句柄
	char m_hexFile_path[MAX_PATH];	// hex配置文件路径

	ultra_radar_display *m_ultra_dis_dlg;
	mmw_radar_display *m_mmw_dis_dlg;
	CMFCButton m_btn_ultra_dis_handle;		// 超声波雷达示意图按钮
	CMFCButton m_btn_mmw_dis_handle;		// 毫米波雷达示意图按钮

	CComboBox m_projct_comb_handle;
	CComboBox m_prodcr_comb_handle;

	Common_Config m_4g_cfg;
	Common_Config m_gps_cfg;
	Common_Config m_imu_cfg;
	Common_Config m_dbl_cam_cfg;
	Common_Config m_sgl_cam_cfg;
	Common_Config m_car_dvr_cfg;
	Common_Config m_brk_act_cfg;

	Ultra_Config m_ultra_front_cfg;
	Ultra_Config m_ultra_end_cfg;
	Ultra_Config m_ultra_left_cfg;
	Ultra_Config m_ultra_right_cfg;

	MMW_Config m_mmw_front_cfg;
	MMW_Config m_mmw_end_cfg;
	MMW_Config m_mmw_left_cfg;
	MMW_Config m_mmw_right_cfg;


	//CStatic m_ultra_front_num;				// 超声波雷达个数显示
	//CStatic m_ultra_end_num;
	//CStatic m_ultra_left_num;
	//CStatic m_ultra_right_num;

	// 配置参数
	Config_Param m_cfg_p;

	// 生成hex文件时用到配置参数
	Sensor_Hex_MGT m_hex_p;

	//bool mySetFontSize(int ID, float times);
	void Init_Widget_Front_Size();
	bool Init_Default_Parameters();
	bool Load_Config_Param_Ini_File(Config_Param* cfg_p);
	void Debug_Print_Cfg_Param_Info(Config_Param cfg_p);
	void Init_Default_Widget_Property();
	void Set_MFCButton_Property(CMFCButton* btn_ppty, int color_flag = 1, bool move_stereo = true);
	void Load_Sensor_Producer_Info_To_Combox();
	void Load_Radar_Status_To_MFCButton();
	void Load_Ultra_Controler_Id_To_Combox();
	void Load_Func_SW_To_MFCButton();

	// 读取配置参数ini文件
	bool Get_Ini_Config_Param_From_UI();
	bool Read_Ini_Config_File();
	bool Write_Ini_Config_File();

	// 生成hex文件
	void Ini_File_Config_Param_To_Hex_Config_Param(uint8_t* data);
	bool Generating_Hex_File();
	bool Make_Hex_Format_API(uint8_t dataLen, uint16_t addr, uint8_t dataType, uint8_t* data, uint8_t* ret_data_str);
	bool Make_Hex_File(uint8_t* data_w, int data_len);

	BOOL File_Is_Exist(LPCTSTR lpszFile);
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedBtn4gStart();
	afx_msg void OnBnClickedBtnGpsStart();
	afx_msg void OnBnClickedBtnImuStart();
	afx_msg void OnBnClickedBtnDblCameraStart();
	afx_msg void OnBnClickedBtnSglCameraStart();
	afx_msg void OnBnClickedBtnCarDvrStart();
	afx_msg void OnBnClickedBtnBrkActStart();
	afx_msg void OnBnClickedBtnUltraFrontStart();
	afx_msg void OnBnClickedBtnUltraEndStart();
	afx_msg void OnBnClickedBtnUltraLeftStart();
	afx_msg void OnBnClickedBtnUltraRightStart();
	afx_msg void OnBnClickedBtnMmwFrontStart();
	afx_msg void OnBnClickedBtnMmwEndStart();
	afx_msg void OnBnClickedBtnMmwLeftStart();
	afx_msg void OnBnClickedBtnMmwRightStart();


	afx_msg void OnBnClickedBtnUltraFront1();
	afx_msg void OnBnClickedBtnUltraFront2();
	afx_msg void OnBnClickedBtnUltraFront3();
	afx_msg void OnBnClickedBtnUltraFront4();
	afx_msg void OnBnClickedBtnUltraFront5();
	afx_msg void OnBnClickedBtnUltraFront6();
	afx_msg void OnBnClickedBtnUltraEnd1();
	afx_msg void OnBnClickedBtnUltraEnd2();
	afx_msg void OnBnClickedBtnUltraEnd3();
	afx_msg void OnBnClickedBtnUltraEnd4();
	afx_msg void OnBnClickedBtnUltraEnd5();
	afx_msg void OnBnClickedBtnUltraEnd6();
	afx_msg void OnBnClickedBtnUltraLeft1();
	afx_msg void OnBnClickedBtnUltraLeft2();
	afx_msg void OnBnClickedBtnUltraLeft3();
	afx_msg void OnBnClickedBtnUltraLeft4();
	afx_msg void OnBnClickedBtnUltraLeft5();
	afx_msg void OnBnClickedBtnUltraLeft6();
	afx_msg void OnBnClickedBtnUltraRight1();
	afx_msg void OnBnClickedBtnUltraRight2();
	afx_msg void OnBnClickedBtnUltraRight3();
	afx_msg void OnBnClickedBtnUltraRight4();
	afx_msg void OnBnClickedBtnUltraRight5();
	afx_msg void OnBnClickedBtnUltraRight6();
	afx_msg void OnBnClickedBtnMmwFront1();
	afx_msg void OnBnClickedBtnMmwEnd1();
	afx_msg void OnBnClickedBtnMmwLeft1();
	afx_msg void OnBnClickedBtnMmwLeft2();
	afx_msg void OnBnClickedBtnMmwRight1();
	afx_msg void OnBnClickedBtnMmwRight2();
	afx_msg void OnBnClickedMbtnUltraDis();
	afx_msg void OnBnClickedMbtnMmwDis();


	//CComboBox m_ultra_front_ctrler_handle;
	CMFCButton m_mfcbtn_write_handle;
	afx_msg void OnBnClickedMfcbtnGenerate();
	CEdit m_edt_hex_name_handle;
	CStatic m_sta_result_show_handle;
};
