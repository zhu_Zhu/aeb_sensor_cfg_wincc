﻿
// Sensor_Param_HexDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "Sensor_Param_Hex.h"
#include "Sensor_Param_HexDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSensorParamHexDlg 对话框



CSensorParamHexDlg::CSensorParamHexDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SENSOR_PARAM_HEX_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}


CSensorParamHexDlg::~CSensorParamHexDlg()
{
	if (m_ultra_dis_dlg) {
		delete m_ultra_dis_dlg;
		m_ultra_dis_dlg = NULL;
	}

	if (m_mmw_dis_dlg) {
		delete m_mmw_dis_dlg;
		m_mmw_dis_dlg = NULL;
	}
	if (l_front) {
		delete l_front;
		l_front = NULL;
	}
}


void CSensorParamHexDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_4G_Start, m_4g_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_GPS_Start, m_gps_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_IMU_Start, m_imu_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_Dbl_Camera_Start, m_dbl_cam_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_Sgl_Camera_Start, m_sgl_cam_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_Car_DVR_Start, m_car_dvr_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_BRK_Act_Start, m_brk_act_cfg.m_func_btn_handle);

	DDX_Control(pDX, IDC_BTN_Ultra_Front_Start, m_ultra_front_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Front_1, m_ultra_front_cfg.m_rdr1_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Front_2, m_ultra_front_cfg.m_rdr2_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Front_3, m_ultra_front_cfg.m_rdr3_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Front_4, m_ultra_front_cfg.m_rdr4_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Front_5, m_ultra_front_cfg.m_rdr5_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Front_6, m_ultra_front_cfg.m_rdr6_btn_handle);


	DDX_Control(pDX, IDC_BTN_Ultra_End_Start, m_ultra_end_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_End_1, m_ultra_end_cfg.m_rdr1_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_End_2, m_ultra_end_cfg.m_rdr2_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_End_3, m_ultra_end_cfg.m_rdr3_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_End_4, m_ultra_end_cfg.m_rdr4_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_End_5, m_ultra_end_cfg.m_rdr5_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_End_6, m_ultra_end_cfg.m_rdr6_btn_handle);

	DDX_Control(pDX, IDC_BTN_Ultra_Left_Start, m_ultra_left_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Left_1, m_ultra_left_cfg.m_rdr1_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Left_2, m_ultra_left_cfg.m_rdr2_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Left_3, m_ultra_left_cfg.m_rdr3_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Left_4, m_ultra_left_cfg.m_rdr4_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Left_5, m_ultra_left_cfg.m_rdr5_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Left_6, m_ultra_left_cfg.m_rdr6_btn_handle);

	DDX_Control(pDX, IDC_BTN_Ultra_Right_Start, m_ultra_right_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Right_1, m_ultra_right_cfg.m_rdr1_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Right_2, m_ultra_right_cfg.m_rdr2_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Right_3, m_ultra_right_cfg.m_rdr3_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Right_4, m_ultra_right_cfg.m_rdr4_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Right_5, m_ultra_right_cfg.m_rdr5_btn_handle);
	DDX_Control(pDX, IDC_BTN_Ultra_Right_6, m_ultra_right_cfg.m_rdr6_btn_handle);

	DDX_Control(pDX, IDC_BTN_MMW_Front_Start, m_mmw_front_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_MMW_Front_1, m_mmw_front_cfg.m_rdr1_btn_handle);

	DDX_Control(pDX, IDC_BTN_MMW_End_Start, m_mmw_end_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_MMW_End_1, m_mmw_end_cfg.m_rdr1_btn_handle);

	DDX_Control(pDX, IDC_BTN_MMW_Left_Start, m_mmw_left_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_MMW_Left_1, m_mmw_left_cfg.m_rdr1_btn_handle);
	DDX_Control(pDX, IDC_BTN_MMW_Left_2, m_mmw_left_cfg.m_rdr2_btn_handle);

	DDX_Control(pDX, IDC_BTN_MMW_Right_Start, m_mmw_right_cfg.m_func_btn_handle);
	DDX_Control(pDX, IDC_BTN_MMW_Right_1, m_mmw_right_cfg.m_rdr1_btn_handle);
	DDX_Control(pDX, IDC_BTN_MMW_Right_2, m_mmw_right_cfg.m_rdr2_btn_handle);

	DDX_Control(pDX, IDC_MBTN_ULTRA_DIS, m_btn_ultra_dis_handle);
	DDX_Control(pDX, IDC_MBTN_MMW_DIS, m_btn_mmw_dis_handle);

	DDX_Control(pDX, IDC_COMB_4G_Maker, m_4g_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_GPS_Maker, m_gps_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_IMU_Maker, m_imu_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_Dbl_Camera_Maker, m_dbl_cam_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_Sgl_Camera_Maker, m_sgl_cam_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_Car_DVR_Maker, m_car_dvr_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_BRK_Act_Maker, m_brk_act_cfg.m_prodcr_comb_handle);

	DDX_Control(pDX, IDC_COMB_Ultra_Front_Maker, m_ultra_front_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_Ultra_End_Maker, m_ultra_end_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_Ultra_Left_Maker, m_ultra_left_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_Ultra_Right_Maker, m_ultra_right_cfg.m_prodcr_comb_handle);

	DDX_Control(pDX, IDC_COMB_MMW_Front_Maker, m_mmw_front_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_MMW_End_Maker, m_mmw_end_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_MMW_Left_Maker, m_mmw_left_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_MMW_Right_Maker, m_mmw_right_cfg.m_prodcr_comb_handle);
	DDX_Control(pDX, IDC_COMB_Project_Maker, m_projct_comb_handle);
	DDX_Control(pDX, IDC_COMB_Product_Maker, m_prodcr_comb_handle);

	DDX_Control(pDX, IDC_COMB_Ultra_Front_Controler, m_ultra_front_cfg.m_ctrler_comb_handle);
	DDX_Control(pDX, IDC_COMB_Ultra_End_Controler, m_ultra_end_cfg.m_ctrler_comb_handle);
	DDX_Control(pDX, IDC_COMB_Ultra_Left_Controler, m_ultra_left_cfg.m_ctrler_comb_handle);
	DDX_Control(pDX, IDC_COMB_Ultra_Right_Controler, m_ultra_right_cfg.m_ctrler_comb_handle);
	DDX_Control(pDX, IDC_MBTN_GENERATE, m_mfcbtn_write_handle);
	DDX_Control(pDX, IDC_EDT_HEX_Name, m_edt_hex_name_handle);
	DDX_Control(pDX, IDC_STA_Result_Show, m_sta_result_show_handle);
}

BEGIN_MESSAGE_MAP(CSensorParamHexDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BTN_4G_Start, &CSensorParamHexDlg::OnBnClickedBtn4gStart)
	ON_BN_CLICKED(IDC_BTN_GPS_Start, &CSensorParamHexDlg::OnBnClickedBtnGpsStart)
	ON_BN_CLICKED(IDC_BTN_IMU_Start, &CSensorParamHexDlg::OnBnClickedBtnImuStart)
	ON_BN_CLICKED(IDC_BTN_Dbl_Camera_Start, &CSensorParamHexDlg::OnBnClickedBtnDblCameraStart)
	ON_BN_CLICKED(IDC_BTN_Sgl_Camera_Start, &CSensorParamHexDlg::OnBnClickedBtnSglCameraStart)
	ON_BN_CLICKED(IDC_BTN_Car_DVR_Start, &CSensorParamHexDlg::OnBnClickedBtnCarDvrStart)
	ON_BN_CLICKED(IDC_BTN_BRK_Act_Start, &CSensorParamHexDlg::OnBnClickedBtnBrkActStart)
	ON_BN_CLICKED(IDC_BTN_Ultra_Front_Start, &CSensorParamHexDlg::OnBnClickedBtnUltraFrontStart)
	ON_BN_CLICKED(IDC_BTN_Ultra_End_Start, &CSensorParamHexDlg::OnBnClickedBtnUltraEndStart)
	ON_BN_CLICKED(IDC_BTN_Ultra_Left_Start, &CSensorParamHexDlg::OnBnClickedBtnUltraLeftStart)
	ON_BN_CLICKED(IDC_BTN_Ultra_Right_Start, &CSensorParamHexDlg::OnBnClickedBtnUltraRightStart)
	ON_BN_CLICKED(IDC_BTN_MMW_Front_Start, &CSensorParamHexDlg::OnBnClickedBtnMmwFrontStart)
	ON_BN_CLICKED(IDC_BTN_MMW_End_Start, &CSensorParamHexDlg::OnBnClickedBtnMmwEndStart)
	ON_BN_CLICKED(IDC_BTN_MMW_Left_Start, &CSensorParamHexDlg::OnBnClickedBtnMmwLeftStart)
	ON_BN_CLICKED(IDC_BTN_MMW_Right_Start, &CSensorParamHexDlg::OnBnClickedBtnMmwRightStart)
	ON_BN_CLICKED(IDC_BTN_Ultra_Front_1, &CSensorParamHexDlg::OnBnClickedBtnUltraFront1)
	ON_BN_CLICKED(IDC_BTN_Ultra_Front_2, &CSensorParamHexDlg::OnBnClickedBtnUltraFront2)
	ON_BN_CLICKED(IDC_BTN_Ultra_Front_3, &CSensorParamHexDlg::OnBnClickedBtnUltraFront3)
	ON_BN_CLICKED(IDC_BTN_Ultra_Front_4, &CSensorParamHexDlg::OnBnClickedBtnUltraFront4)
	ON_BN_CLICKED(IDC_BTN_Ultra_Front_5, &CSensorParamHexDlg::OnBnClickedBtnUltraFront5)
	ON_BN_CLICKED(IDC_BTN_Ultra_Front_6, &CSensorParamHexDlg::OnBnClickedBtnUltraFront6)
	ON_BN_CLICKED(IDC_BTN_Ultra_End_1, &CSensorParamHexDlg::OnBnClickedBtnUltraEnd1)
	ON_BN_CLICKED(IDC_BTN_Ultra_End_2, &CSensorParamHexDlg::OnBnClickedBtnUltraEnd2)
	ON_BN_CLICKED(IDC_BTN_Ultra_End_3, &CSensorParamHexDlg::OnBnClickedBtnUltraEnd3)
	ON_BN_CLICKED(IDC_BTN_Ultra_End_4, &CSensorParamHexDlg::OnBnClickedBtnUltraEnd4)
	ON_BN_CLICKED(IDC_BTN_Ultra_End_5, &CSensorParamHexDlg::OnBnClickedBtnUltraEnd5)
	ON_BN_CLICKED(IDC_BTN_Ultra_End_6, &CSensorParamHexDlg::OnBnClickedBtnUltraEnd6)
	ON_BN_CLICKED(IDC_BTN_Ultra_Left_1, &CSensorParamHexDlg::OnBnClickedBtnUltraLeft1)
	ON_BN_CLICKED(IDC_BTN_Ultra_Left_2, &CSensorParamHexDlg::OnBnClickedBtnUltraLeft2)
	ON_BN_CLICKED(IDC_BTN_Ultra_Left_3, &CSensorParamHexDlg::OnBnClickedBtnUltraLeft3)
	ON_BN_CLICKED(IDC_BTN_Ultra_Left_4, &CSensorParamHexDlg::OnBnClickedBtnUltraLeft4)
	ON_BN_CLICKED(IDC_BTN_Ultra_Left_5, &CSensorParamHexDlg::OnBnClickedBtnUltraLeft5)
	ON_BN_CLICKED(IDC_BTN_Ultra_Left_6, &CSensorParamHexDlg::OnBnClickedBtnUltraLeft6)
	ON_BN_CLICKED(IDC_BTN_Ultra_Right_1, &CSensorParamHexDlg::OnBnClickedBtnUltraRight1)
	ON_BN_CLICKED(IDC_BTN_Ultra_Right_2, &CSensorParamHexDlg::OnBnClickedBtnUltraRight2)
	ON_BN_CLICKED(IDC_BTN_Ultra_Right_3, &CSensorParamHexDlg::OnBnClickedBtnUltraRight3)
	ON_BN_CLICKED(IDC_BTN_Ultra_Right_4, &CSensorParamHexDlg::OnBnClickedBtnUltraRight4)
	ON_BN_CLICKED(IDC_BTN_Ultra_Right_5, &CSensorParamHexDlg::OnBnClickedBtnUltraRight5)
	ON_BN_CLICKED(IDC_BTN_Ultra_Right_6, &CSensorParamHexDlg::OnBnClickedBtnUltraRight6)
	ON_BN_CLICKED(IDC_BTN_MMW_Front_1, &CSensorParamHexDlg::OnBnClickedBtnMmwFront1)
	ON_BN_CLICKED(IDC_BTN_MMW_End_1, &CSensorParamHexDlg::OnBnClickedBtnMmwEnd1)
	ON_BN_CLICKED(IDC_BTN_MMW_Left_1, &CSensorParamHexDlg::OnBnClickedBtnMmwLeft1)
	ON_BN_CLICKED(IDC_BTN_MMW_Left_2, &CSensorParamHexDlg::OnBnClickedBtnMmwLeft2)
	ON_BN_CLICKED(IDC_BTN_MMW_Right_1, &CSensorParamHexDlg::OnBnClickedBtnMmwRight1)
	ON_BN_CLICKED(IDC_BTN_MMW_Right_2, &CSensorParamHexDlg::OnBnClickedBtnMmwRight2)
	ON_BN_CLICKED(IDC_MBTN_ULTRA_DIS, &CSensorParamHexDlg::OnBnClickedMbtnUltraDis)
	ON_BN_CLICKED(IDC_MBTN_MMW_DIS, &CSensorParamHexDlg::OnBnClickedMbtnMmwDis)
	ON_BN_CLICKED(IDC_MBTN_GENERATE, &CSensorParamHexDlg::OnBnClickedMfcbtnGenerate)
END_MESSAGE_MAP()


// CSensorParamHexDlg 消息处理程序
BOOL CSensorParamHexDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	//AllocConsole();
	//freopen("CONOUT$", "w",stdout);
	//printf("调试打印");
	//SetBackgroundColor(RGB(0,0,255));
	SetBackgroundColor(RGB(19,130,230));		// 深蓝色
	SetBackgroundColor(RGB(96,174,242));		// 淡蓝色
	//SetBackgroundColor(RGB(170,214,238));		// 浅蓝灰色
	//SetBackgroundColor(RGB(120,163,222));		// 浅色蓝色1
	SetBackgroundColor(RGB(147,193,254));		// 浅色蓝色2

	if (Init_Default_Parameters()) {
		Init_Default_Widget_Property();
	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CSensorParamHexDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CSensorParamHexDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CSensorParamHexDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSensorParamHexDlg::Set_MFCButton_Property(CMFCButton* btn_ppty, int flag, bool move_stereo)
{
	btn_ppty->m_bTransparent = false;
	btn_ppty->m_bDontUseWinXPTheme = true;
	btn_ppty->m_bDrawFocus = false;								// 消除按钮外围黑圈
	if (move_stereo) {
		btn_ppty->m_nFlatStyle = CMFCButton::BUTTONSTYLE_NOBORDERS;	// 去除立体效果
	}
	switch (flag) {
	case 0: {btn_ppty->SetFaceColor(DEFAULT_COLOUR); }break;	// 设置默认颜色
	case 1: {btn_ppty->SetFaceColor(DISABLE_COLOUR); }break;	// 设置红色颜色
	case 2: {btn_ppty->SetFaceColor(ENABLE_COLOUR); }break;		// 设置绿色颜色
	}
}

bool CSensorParamHexDlg::Init_Default_Parameters()
{
	m_4g_cfg.m_func_btn_sw = false;
	m_gps_cfg.m_func_btn_sw = false;
	m_imu_cfg.m_func_btn_sw = false;
	m_dbl_cam_cfg.m_func_btn_sw = false;
	m_sgl_cam_cfg.m_func_btn_sw = false;
	m_car_dvr_cfg.m_func_btn_sw = false;
	m_brk_act_cfg.m_func_btn_sw = false;

	m_ultra_front_cfg.m_func_btn_sw = false;
	m_ultra_front_cfg.m_rdr1_btn_sw = false;
	m_ultra_front_cfg.m_rdr2_btn_sw = false;
	m_ultra_front_cfg.m_rdr3_btn_sw = false;
	m_ultra_front_cfg.m_rdr4_btn_sw = false;
	m_ultra_front_cfg.m_rdr5_btn_sw = false;
	m_ultra_front_cfg.m_rdr6_btn_sw = false;

	m_ultra_end_cfg.m_func_btn_sw = false;
	m_ultra_end_cfg.m_rdr1_btn_sw = false;
	m_ultra_end_cfg.m_rdr2_btn_sw = false;
	m_ultra_end_cfg.m_rdr3_btn_sw = false;
	m_ultra_end_cfg.m_rdr4_btn_sw = false;
	m_ultra_end_cfg.m_rdr5_btn_sw = false;
	m_ultra_end_cfg.m_rdr6_btn_sw = false;

	m_ultra_left_cfg.m_func_btn_sw = false;
	m_ultra_left_cfg.m_rdr1_btn_sw = false;
	m_ultra_left_cfg.m_rdr2_btn_sw = false;
	m_ultra_left_cfg.m_rdr3_btn_sw = false;
	m_ultra_left_cfg.m_rdr4_btn_sw = false;
	m_ultra_left_cfg.m_rdr5_btn_sw = false;
	m_ultra_left_cfg.m_rdr6_btn_sw = false;

	m_ultra_right_cfg.m_func_btn_sw = false;
	m_ultra_right_cfg.m_rdr1_btn_sw = false;
	m_ultra_right_cfg.m_rdr2_btn_sw = false;
	m_ultra_right_cfg.m_rdr3_btn_sw = false;
	m_ultra_right_cfg.m_rdr4_btn_sw = false;
	m_ultra_right_cfg.m_rdr5_btn_sw = false;
	m_ultra_right_cfg.m_rdr6_btn_sw = false;


	m_mmw_front_cfg.m_func_btn_sw = false;
	m_mmw_front_cfg.m_rdr1_btn_sw = false;

	m_mmw_end_cfg.m_func_btn_sw = false;
	m_mmw_end_cfg.m_rdr1_btn_sw = false;

	m_mmw_left_cfg.m_func_btn_sw = false;
	m_mmw_left_cfg.m_rdr1_btn_sw = false;
	m_mmw_left_cfg.m_rdr2_btn_sw = false;

	m_mmw_right_cfg.m_func_btn_sw = false;
	m_mmw_right_cfg.m_rdr1_btn_sw = false;
	m_mmw_right_cfg.m_rdr2_btn_sw = false;

	m_ultra_dis_btn_sw = false;
	m_mmw_dis_btn_sw = false;

	m_ultra_front_cfg.rdr_num = 0;
	m_ultra_end_cfg.rdr_num = 0;
	m_ultra_left_cfg.rdr_num = 0;
	m_ultra_right_cfg.rdr_num = 0;

	// 加载配置参数，读取ini文件
	if (Load_Config_Param_Ini_File(&m_cfg_p)) {
		// 打印
		Debug_Print_Cfg_Param_Info(m_cfg_p);
		return true;
	}
	return false;
}


int m_set_id_front[] = { IDC_COMB_Project_Maker, IDC_STA_Project, \
	IDC_COMB_Product_Maker , IDC_STA_Product, \
	IDC_BTN_4G_Start ,IDC_COMB_4G_Maker ,IDC_STA_4G , \
	IDC_BTN_GPS_Start ,IDC_COMB_GPS_Maker ,IDC_STA_GPS , \
	IDC_BTN_IMU_Start ,IDC_COMB_IMU_Maker ,IDC_STA_IMU , \
	IDC_STA_Dbl_Cam, IDC_BTN_Dbl_Camera_Start, IDC_COMB_Dbl_Camera_Maker, \
	IDC_STA_Sgl_Cam, IDC_BTN_Sgl_Camera_Start, IDC_COMB_Sgl_Camera_Maker, \
	IDC_STA_Car_Dvr, IDC_BTN_Car_DVR_Start, IDC_COMB_Car_DVR_Maker, \
	IDC_STA_Brk_Act, IDC_BTN_BRK_Act_Start, IDC_COMB_BRK_Act_Maker,\
	/*25*/

	IDC_STA_Ultra_Front, IDC_BTN_Ultra_Front_Start, IDC_COMB_Ultra_Front_Maker, IDC_COMB_Ultra_Front_Controler, \
	IDC_BTN_Ultra_Front_1, IDC_BTN_Ultra_Front_2, IDC_BTN_Ultra_Front_3,IDC_BTN_Ultra_Front_4, IDC_BTN_Ultra_Front_5,IDC_BTN_Ultra_Front_6,\

	IDC_STA_Ultra_End, IDC_BTN_Ultra_End_Start, IDC_COMB_Ultra_End_Maker, IDC_COMB_Ultra_End_Controler, \
	IDC_BTN_Ultra_End_1, IDC_BTN_Ultra_End_2, IDC_BTN_Ultra_End_3, IDC_BTN_Ultra_End_4, IDC_BTN_Ultra_End_5, IDC_BTN_Ultra_End_6,\

	IDC_STA_Ultra_Left, IDC_BTN_Ultra_Left_Start, IDC_COMB_Ultra_Left_Maker, IDC_COMB_Ultra_Left_Controler, \
	IDC_BTN_Ultra_Left_1, IDC_BTN_Ultra_Left_2, IDC_BTN_Ultra_Left_3, IDC_BTN_Ultra_Left_4, IDC_BTN_Ultra_Left_5, IDC_BTN_Ultra_Left_6,\

	IDC_STA_Ultra_Right, IDC_BTN_Ultra_Right_Start, IDC_COMB_Ultra_Right_Maker, IDC_COMB_Ultra_Right_Controler, \
	IDC_BTN_Ultra_Right_1, IDC_BTN_Ultra_Right_2, IDC_BTN_Ultra_Right_3, IDC_BTN_Ultra_Right_4, IDC_BTN_Ultra_Right_5, IDC_BTN_Ultra_Right_6,\
	/*65*/

	IDC_STA_MMW_Front, IDC_BTN_MMW_Front_Start, IDC_COMB_MMW_Front_Maker, IDC_BTN_MMW_Front_1,\
	IDC_STA_MMW_End, IDC_BTN_MMW_End_Start, IDC_COMB_MMW_End_Maker, IDC_BTN_MMW_End_1,\
	IDC_STA_MMW_Left, IDC_BTN_MMW_Left_Start, IDC_COMB_MMW_Left_Maker, IDC_BTN_MMW_Left_1, IDC_BTN_MMW_Left_2,\
	IDC_STA_MMW_Right, IDC_BTN_MMW_Right_Start, IDC_COMB_MMW_Right_Maker, IDC_BTN_MMW_Right_1, IDC_BTN_MMW_Right_2,\
	/*83*/
	IDC_GRP_Ultra, IDC_GRP_MMW, IDC_MBTN_ULTRA_DIS, IDC_MBTN_MMW_DIS,\
	IDC_STA_Hex_Msg, IDC_EDT_HEX_Name, IDC_MBTN_GENERATE, IDC_STA_Result_Show
};

void CSensorParamHexDlg::Init_Widget_Front_Size()
{
	l_front = new CFont;
	l_front->CreateFont(MY_FRONT_SIZE,	// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_NORMAL,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
		_T("黑体"));					// lpszFac Arial Consolas 黑体

	for (int i = 0; i < 91;i++) {
		GetDlgItem(m_set_id_front[i])->SetFont(l_front);
	}
}
void CSensorParamHexDlg::Init_Default_Widget_Property()
{
	// 控件字体大小
	Init_Widget_Front_Size();

	// 功能开关
	m_4g_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_gps_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_imu_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_dbl_cam_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_sgl_cam_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_car_dvr_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_brk_act_cfg.m_func_btn_handle.SetWindowText("功能关闭");

	m_ultra_front_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_ultra_end_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_ultra_left_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_ultra_right_cfg.m_func_btn_handle.SetWindowText("功能关闭");

	m_mmw_front_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_mmw_end_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_mmw_left_cfg.m_func_btn_handle.SetWindowText("功能关闭");
	m_mmw_right_cfg.m_func_btn_handle.SetWindowText("功能关闭");

	// 按钮状态
	m_4g_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_gps_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_imu_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_dbl_cam_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_sgl_cam_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_car_dvr_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_brk_act_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);

	m_ultra_front_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_ultra_front_cfg.m_ctrler_comb_handle.ShowWindow(SW_HIDE);
	m_ultra_front_cfg.m_rdr1_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_front_cfg.m_rdr2_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_front_cfg.m_rdr3_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_front_cfg.m_rdr4_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_front_cfg.m_rdr5_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_front_cfg.m_rdr6_btn_handle.ShowWindow(SW_HIDE);

	m_ultra_end_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_ultra_end_cfg.m_ctrler_comb_handle.ShowWindow(SW_HIDE);
	m_ultra_end_cfg.m_rdr1_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_end_cfg.m_rdr2_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_end_cfg.m_rdr3_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_end_cfg.m_rdr4_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_end_cfg.m_rdr5_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_end_cfg.m_rdr6_btn_handle.ShowWindow(SW_HIDE);

	m_ultra_left_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_ultra_left_cfg.m_ctrler_comb_handle.ShowWindow(SW_HIDE);
	m_ultra_left_cfg.m_rdr1_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_left_cfg.m_rdr2_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_left_cfg.m_rdr3_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_left_cfg.m_rdr4_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_left_cfg.m_rdr5_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_left_cfg.m_rdr6_btn_handle.ShowWindow(SW_HIDE);

	m_ultra_right_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_ultra_right_cfg.m_ctrler_comb_handle.ShowWindow(SW_HIDE);
	m_ultra_right_cfg.m_rdr1_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_right_cfg.m_rdr2_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_right_cfg.m_rdr3_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_right_cfg.m_rdr4_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_right_cfg.m_rdr5_btn_handle.ShowWindow(SW_HIDE);
	m_ultra_right_cfg.m_rdr6_btn_handle.ShowWindow(SW_HIDE);

	m_mmw_front_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_mmw_front_cfg.m_rdr1_btn_handle.ShowWindow(SW_HIDE);

	m_mmw_end_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_mmw_end_cfg.m_rdr1_btn_handle.ShowWindow(SW_HIDE);

	m_mmw_left_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_mmw_left_cfg.m_rdr1_btn_handle.ShowWindow(SW_HIDE);
	m_mmw_left_cfg.m_rdr2_btn_handle.ShowWindow(SW_HIDE);

	m_mmw_right_cfg.m_prodcr_comb_handle.ShowWindow(SW_HIDE);
	m_mmw_right_cfg.m_rdr1_btn_handle.ShowWindow(SW_HIDE);
	m_mmw_right_cfg.m_rdr2_btn_handle.ShowWindow(SW_HIDE);

	// 清空combox显示内容(类型或者厂家名字)
	m_projct_comb_handle.ResetContent();
	m_prodcr_comb_handle.ResetContent();

	m_4g_cfg.m_prodcr_comb_handle.ResetContent();
	m_gps_cfg.m_prodcr_comb_handle.ResetContent();
	m_imu_cfg.m_prodcr_comb_handle.ResetContent();
	m_dbl_cam_cfg.m_prodcr_comb_handle.ResetContent();
	m_sgl_cam_cfg.m_prodcr_comb_handle.ResetContent();
	m_car_dvr_cfg.m_prodcr_comb_handle.ResetContent();
	m_brk_act_cfg.m_prodcr_comb_handle.ResetContent();

	m_ultra_front_cfg.m_prodcr_comb_handle.ResetContent();
	m_ultra_end_cfg.m_prodcr_comb_handle.ResetContent();
	m_ultra_left_cfg.m_prodcr_comb_handle.ResetContent();
	m_ultra_right_cfg.m_prodcr_comb_handle.ResetContent();

	m_mmw_front_cfg.m_prodcr_comb_handle.ResetContent();
	m_mmw_end_cfg.m_prodcr_comb_handle.ResetContent();
	m_mmw_left_cfg.m_prodcr_comb_handle.ResetContent();
	m_mmw_right_cfg.m_prodcr_comb_handle.ResetContent();

	// 清空超声波雷达个数
	m_ultra_front_cfg.m_ctrler_comb_handle.ResetContent();
	m_ultra_end_cfg.m_ctrler_comb_handle.ResetContent();
	m_ultra_left_cfg.m_ctrler_comb_handle.ResetContent();
	m_ultra_right_cfg.m_ctrler_comb_handle.ResetContent();

	// 2张图片句柄
	m_ultra_dis_dlg = new ultra_radar_display();
	m_ultra_dis_dlg->Create(IDD_DLG_ULTRAL_DIS, this);

	m_mmw_dis_dlg = new mmw_radar_display();
	m_mmw_dis_dlg->Create(IDD_DLG_WWM_DIS, this);

	Set_MFCButton_Property(&m_btn_ultra_dis_handle, 0, false);
	Set_MFCButton_Property(&m_btn_mmw_dis_handle, 0, false);

	// 设置按钮属性，让其显示颜色
	Set_MFCButton_Property(&m_4g_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_gps_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_imu_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_dbl_cam_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_sgl_cam_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_car_dvr_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_brk_act_cfg.m_func_btn_handle);

	Set_MFCButton_Property(&m_ultra_front_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_ultra_front_cfg.m_rdr1_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_front_cfg.m_rdr2_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_front_cfg.m_rdr3_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_front_cfg.m_rdr4_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_front_cfg.m_rdr5_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_front_cfg.m_rdr6_btn_handle, 0);

	Set_MFCButton_Property(&m_ultra_end_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_ultra_end_cfg.m_rdr1_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_end_cfg.m_rdr2_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_end_cfg.m_rdr3_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_end_cfg.m_rdr4_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_end_cfg.m_rdr5_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_end_cfg.m_rdr6_btn_handle, 0);

	Set_MFCButton_Property(&m_ultra_left_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_ultra_left_cfg.m_rdr1_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_left_cfg.m_rdr2_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_left_cfg.m_rdr3_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_left_cfg.m_rdr4_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_left_cfg.m_rdr5_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_left_cfg.m_rdr6_btn_handle, 0);

	Set_MFCButton_Property(&m_ultra_right_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_ultra_right_cfg.m_rdr1_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_right_cfg.m_rdr2_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_right_cfg.m_rdr3_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_right_cfg.m_rdr4_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_right_cfg.m_rdr5_btn_handle, 0);
	Set_MFCButton_Property(&m_ultra_right_cfg.m_rdr6_btn_handle, 0);

	Set_MFCButton_Property(&m_mmw_front_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_mmw_front_cfg.m_rdr1_btn_handle, 0);

	Set_MFCButton_Property(&m_mmw_end_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_mmw_end_cfg.m_rdr1_btn_handle, 0);

	Set_MFCButton_Property(&m_mmw_left_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_mmw_left_cfg.m_rdr1_btn_handle, 0);
	Set_MFCButton_Property(&m_mmw_left_cfg.m_rdr2_btn_handle, 0);

	Set_MFCButton_Property(&m_mmw_right_cfg.m_func_btn_handle);
	Set_MFCButton_Property(&m_mmw_right_cfg.m_rdr1_btn_handle, 0);
	Set_MFCButton_Property(&m_mmw_right_cfg.m_rdr2_btn_handle, 0);

	// 加载功能开关MFCButton中
	Load_Func_SW_To_MFCButton();
	
	// 加载combox内容
	Load_Sensor_Producer_Info_To_Combox();

	// 加载按钮状态到MFCButton中
	Load_Radar_Status_To_MFCButton();

	// 加载超声波雷达控制器的编号到combox中
	Load_Ultra_Controler_Id_To_Combox();
}

void CSensorParamHexDlg::Load_Func_SW_To_MFCButton()
{
	m_4g_cfg.m_func_btn_sw = !m_cfg_p.m_4g.m_func_sw;
	m_gps_cfg.m_func_btn_sw = !m_cfg_p.m_gps.m_func_sw;
	m_imu_cfg.m_func_btn_sw = !m_cfg_p.m_imu.m_func_sw;
	m_dbl_cam_cfg.m_func_btn_sw = !m_cfg_p.m_dbl_cam.m_func_sw;
	m_sgl_cam_cfg.m_func_btn_sw = !m_cfg_p.m_sgl_cam.m_func_sw;
	m_car_dvr_cfg.m_func_btn_sw = !m_cfg_p.m_car_dvr.m_func_sw;
	m_brk_act_cfg.m_func_btn_sw = !m_cfg_p.m_brk_act.m_func_sw;

	OnBnClickedBtn4gStart();
	OnBnClickedBtnGpsStart();
	OnBnClickedBtnImuStart();
	OnBnClickedBtnDblCameraStart();
	OnBnClickedBtnSglCameraStart();
	OnBnClickedBtnCarDvrStart();
	OnBnClickedBtnBrkActStart();

	// 超声波雷达
	m_ultra_front_cfg.m_func_btn_sw = !m_cfg_p.m_ultra_front.m_func_sw;
	m_ultra_end_cfg.m_func_btn_sw = !m_cfg_p.m_ultra_end.m_func_sw;
	m_ultra_left_cfg.m_func_btn_sw = !m_cfg_p.m_ultra_left.m_func_sw;
	m_ultra_right_cfg.m_func_btn_sw = !m_cfg_p.m_ultra_right.m_func_sw;
	OnBnClickedBtnUltraFrontStart();
	OnBnClickedBtnUltraEndStart();
	OnBnClickedBtnUltraLeftStart();
	OnBnClickedBtnUltraRightStart();

	// 毫米波雷达
	m_mmw_front_cfg.m_func_btn_sw = !m_cfg_p.m_mmw_front.m_func_sw;
	m_mmw_end_cfg.m_func_btn_sw = !m_cfg_p.m_mmw_end.m_func_sw;
	m_mmw_left_cfg.m_func_btn_sw = !m_cfg_p.m_mmw_left.m_func_sw;
	m_mmw_right_cfg.m_func_btn_sw = !m_cfg_p.m_mmw_right.m_func_sw;
	OnBnClickedBtnMmwFrontStart();
	OnBnClickedBtnMmwEndStart();
	OnBnClickedBtnMmwLeftStart();
	OnBnClickedBtnMmwRightStart();
}
void CSensorParamHexDlg::Load_Sensor_Producer_Info_To_Combox()
{
	// 项目类型
	for (int i = 0; i <= m_cfg_p.m_project_type.m_type_num; i++) {
		m_projct_comb_handle.InsertString(i, m_cfg_p.m_project_type.m_type[i]);
	}
	m_projct_comb_handle.SetCurSel(m_cfg_p.m_project_type.m_type_index);

	// 产品类型
	for (int i = 0; i <= m_cfg_p.m_product_type.m_type_num; i++) {
		m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_product_type.m_type[i]);
	}
	m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_product_type.m_type_index);

	// 4G厂家
	for (int i = 0; i <= m_cfg_p.m_4g.m_producer_num; i++) {
		m_4g_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_4g.m_producer[i]);
	}
	m_4g_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_4g.m_producer_index);

	// GPS厂家
	for (int i = 0; i <= m_cfg_p.m_gps.m_producer_num; i++) {
		m_gps_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_gps.m_producer[i]);
	}
	m_gps_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_gps.m_producer_index);


	// IMU厂家
	for (int i = 0; i <= m_cfg_p.m_imu.m_producer_num; i++) {
		m_imu_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_imu.m_producer[i]);
	}
	m_imu_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_imu.m_producer_index);

	// 双面相机厂家
	for (int i = 0; i <= m_cfg_p.m_dbl_cam.m_producer_num; i++) {
		m_dbl_cam_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_dbl_cam.m_producer[i]);
	}
	m_dbl_cam_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_dbl_cam.m_producer_index);

	// 单目相机厂家
	for (int i = 0; i <= m_cfg_p.m_sgl_cam.m_producer_num; i++) {
		m_sgl_cam_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_sgl_cam.m_producer[i]);
	}
	m_sgl_cam_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_sgl_cam.m_producer_index);

	// 行车记录仪厂家
	for (int i = 0; i <= m_cfg_p.m_car_dvr.m_producer_num; i++) {
		m_car_dvr_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_car_dvr.m_producer[i]);
	}
	m_car_dvr_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_car_dvr.m_producer_index);

	// 刹车执行机构厂家
	for (int i = 0; i <= m_cfg_p.m_brk_act.m_producer_num; i++) {
		m_brk_act_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_brk_act.m_producer[i]);
	}
	m_brk_act_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_brk_act.m_producer_index);

	// 前方超声波雷达厂家
	for (int i = 0; i <= m_cfg_p.m_ultra_front.m_producer_num; i++) {
		m_ultra_front_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_ultra_front.m_producer[i]);
	}
	m_ultra_front_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_ultra_front.m_producer_index);

	// 后方超声波雷达厂家
	for (int i = 0; i <= m_cfg_p.m_ultra_end.m_producer_num; i++) {
		m_ultra_end_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_ultra_end.m_producer[i]);
	}
	m_ultra_end_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_ultra_end.m_producer_index);


	// 左侧超声波雷达厂家
	for (int i = 0; i <= m_cfg_p.m_ultra_left.m_producer_num; i++) {
		m_ultra_left_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_ultra_left.m_producer[i]);
	}
	m_ultra_left_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_ultra_left.m_producer_index);

	// 右侧超声波雷达厂家
	for (int i = 0; i <= m_cfg_p.m_ultra_right.m_producer_num; i++) {
		m_ultra_right_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_ultra_right.m_producer[i]);
	}
	m_ultra_right_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_ultra_right.m_producer_index);


	// 前方毫米波雷达厂家
	for (int i = 0; i <= m_cfg_p.m_mmw_front.m_producer_num; i++) {
		m_mmw_front_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_mmw_front.m_producer[i]);
	}
	m_mmw_front_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_mmw_front.m_producer_index);

	// 后方毫米波雷达厂家
	for (int i = 0; i <= m_cfg_p.m_mmw_end.m_producer_num; i++) {
		m_mmw_end_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_mmw_end.m_producer[i]);
	}
	m_mmw_end_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_mmw_end.m_producer_index);


	// 左侧毫米波雷达厂家
	for (int i = 0; i <= m_cfg_p.m_mmw_left.m_producer_num; i++) {
		m_mmw_left_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_mmw_left.m_producer[i]);
	}
	m_mmw_left_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_mmw_left.m_producer_index);

	// 右侧毫米波雷达厂家
	for (int i = 0; i <= m_cfg_p.m_mmw_right.m_producer_num; i++) {
		m_mmw_right_cfg.m_prodcr_comb_handle.InsertString(i, m_cfg_p.m_mmw_right.m_producer[i]);
	}
	m_mmw_right_cfg.m_prodcr_comb_handle.SetCurSel(m_cfg_p.m_mmw_right.m_producer_index);

}

void CSensorParamHexDlg::Load_Ultra_Controler_Id_To_Combox()
{
	char ctrler_id_str[4] = { 0 };

	// 前方超声波雷达
	for (int i = 0; i < 4; i++) {
		sprintf(ctrler_id_str, "%d", i);
		m_ultra_front_cfg.m_ctrler_comb_handle.InsertString(i, ctrler_id_str);
		m_ultra_end_cfg.m_ctrler_comb_handle.InsertString(i, ctrler_id_str);
		m_ultra_left_cfg.m_ctrler_comb_handle.InsertString(i, ctrler_id_str);
		m_ultra_right_cfg.m_ctrler_comb_handle.InsertString(i, ctrler_id_str);
	}

	m_ultra_front_cfg.m_ctrler_comb_handle.SetCurSel(m_cfg_p.m_ultra_front.controler_id);
	m_ultra_end_cfg.m_ctrler_comb_handle.SetCurSel(m_cfg_p.m_ultra_end.controler_id);
	m_ultra_left_cfg.m_ctrler_comb_handle.SetCurSel(m_cfg_p.m_ultra_left.controler_id);
	m_ultra_right_cfg.m_ctrler_comb_handle.SetCurSel(m_cfg_p.m_ultra_right.controler_id);
}

void CSensorParamHexDlg::Load_Radar_Status_To_MFCButton()
{
	// 前方超声波雷达
	m_ultra_front_cfg.m_rdr1_btn_sw = !m_cfg_p.m_ultra_front.m_rdr1_sw;
	m_ultra_front_cfg.m_rdr2_btn_sw = !m_cfg_p.m_ultra_front.m_rdr2_sw;
	m_ultra_front_cfg.m_rdr3_btn_sw = !m_cfg_p.m_ultra_front.m_rdr3_sw;
	m_ultra_front_cfg.m_rdr4_btn_sw = !m_cfg_p.m_ultra_front.m_rdr4_sw;
	m_ultra_front_cfg.m_rdr5_btn_sw = !m_cfg_p.m_ultra_front.m_rdr5_sw;
	m_ultra_front_cfg.m_rdr6_btn_sw = !m_cfg_p.m_ultra_front.m_rdr6_sw;

	OnBnClickedBtnUltraFront1();
	OnBnClickedBtnUltraFront2();
	OnBnClickedBtnUltraFront3();
	OnBnClickedBtnUltraFront4();
	OnBnClickedBtnUltraFront5();
	OnBnClickedBtnUltraFront6();

	// 后方超声波雷达
	m_ultra_end_cfg.m_rdr1_btn_sw = !m_cfg_p.m_ultra_end.m_rdr1_sw;
	m_ultra_end_cfg.m_rdr2_btn_sw = !m_cfg_p.m_ultra_end.m_rdr2_sw;
	m_ultra_end_cfg.m_rdr3_btn_sw = !m_cfg_p.m_ultra_end.m_rdr3_sw;
	m_ultra_end_cfg.m_rdr4_btn_sw = !m_cfg_p.m_ultra_end.m_rdr4_sw;
	m_ultra_end_cfg.m_rdr5_btn_sw = !m_cfg_p.m_ultra_end.m_rdr5_sw;
	m_ultra_end_cfg.m_rdr6_btn_sw = !m_cfg_p.m_ultra_end.m_rdr6_sw;

	OnBnClickedBtnUltraEnd1();
	OnBnClickedBtnUltraEnd2();
	OnBnClickedBtnUltraEnd3();
	OnBnClickedBtnUltraEnd4();
	OnBnClickedBtnUltraEnd5();
	OnBnClickedBtnUltraEnd6();

	// 左侧超声波雷达
	m_ultra_left_cfg.m_rdr1_btn_sw = !m_cfg_p.m_ultra_left.m_rdr1_sw;
	m_ultra_left_cfg.m_rdr2_btn_sw = !m_cfg_p.m_ultra_left.m_rdr2_sw;
	m_ultra_left_cfg.m_rdr3_btn_sw = !m_cfg_p.m_ultra_left.m_rdr3_sw;
	m_ultra_left_cfg.m_rdr4_btn_sw = !m_cfg_p.m_ultra_left.m_rdr4_sw;
	m_ultra_left_cfg.m_rdr5_btn_sw = !m_cfg_p.m_ultra_left.m_rdr5_sw;
	m_ultra_left_cfg.m_rdr6_btn_sw = !m_cfg_p.m_ultra_left.m_rdr6_sw;

	OnBnClickedBtnUltraLeft1();
	OnBnClickedBtnUltraLeft2();
	OnBnClickedBtnUltraLeft3();
	OnBnClickedBtnUltraLeft4();
	OnBnClickedBtnUltraLeft5();
	OnBnClickedBtnUltraLeft6();

	// 右侧超声波雷达
	m_ultra_right_cfg.m_rdr1_btn_sw = !m_cfg_p.m_ultra_right.m_rdr1_sw;
	m_ultra_right_cfg.m_rdr2_btn_sw = !m_cfg_p.m_ultra_right.m_rdr2_sw;
	m_ultra_right_cfg.m_rdr3_btn_sw = !m_cfg_p.m_ultra_right.m_rdr3_sw;
	m_ultra_right_cfg.m_rdr4_btn_sw = !m_cfg_p.m_ultra_right.m_rdr4_sw;
	m_ultra_right_cfg.m_rdr5_btn_sw = !m_cfg_p.m_ultra_right.m_rdr5_sw;
	m_ultra_right_cfg.m_rdr6_btn_sw = !m_cfg_p.m_ultra_right.m_rdr6_sw;

	OnBnClickedBtnUltraRight1();
	OnBnClickedBtnUltraRight2();
	OnBnClickedBtnUltraRight3();
	OnBnClickedBtnUltraRight4();
	OnBnClickedBtnUltraRight5();
	OnBnClickedBtnUltraRight6();

	// 前方毫米波雷达
	m_mmw_front_cfg.m_rdr1_btn_sw = !m_cfg_p.m_mmw_front.m_rdr1_sw;
	OnBnClickedBtnMmwFront1();

	// 后方毫米波雷达
	m_mmw_end_cfg.m_rdr1_btn_sw = !m_cfg_p.m_mmw_end.m_rdr1_sw;
	OnBnClickedBtnMmwEnd1();

	// 左侧毫米波雷达
	m_mmw_left_cfg.m_rdr1_btn_sw = !m_cfg_p.m_mmw_left.m_rdr1_sw;
	m_mmw_left_cfg.m_rdr2_btn_sw = !m_cfg_p.m_mmw_left.m_rdr2_sw;
	OnBnClickedBtnMmwLeft1();
	OnBnClickedBtnMmwLeft2();

	// 右侧毫米波雷达
	m_mmw_right_cfg.m_rdr1_btn_sw = !m_cfg_p.m_mmw_right.m_rdr1_sw;
	m_mmw_right_cfg.m_rdr2_btn_sw = !m_cfg_p.m_mmw_right.m_rdr2_sw;
	OnBnClickedBtnMmwRight1();
	OnBnClickedBtnMmwRight2();
}

bool CSensorParamHexDlg::Read_Ini_Config_File()
{
	TCHAR szIniPath[MAX_PATH] = {0};
	char szValue[50] = { 0 };
	int intValue = 0;

	// 文件路径
	GetCurrentDirectory(MAX_PATH, szIniPath);
	strcpy(m_iniFile_path, szIniPath);
	strcat(m_iniFile_path, "\\Settings.ini");
	printf("path:%s\n", m_iniFile_path);

	// 判断文件是否存在，不在就直接返回
	if (!File_Is_Exist(m_iniFile_path)) {
		printf("Settings文件不存在.\n");
		m_sta_result_show_handle.SetWindowText("Settings.ini 文件不存在。请先将该文件加载当前路径下。");
		return false;
	}

	m_iniFile_handle.SetIniFilePath(m_iniFile_path);

	char producer_name[30] = { 0 };

	// 项目类型
	m_iniFile_handle.GetString("PROJECT_TYPE", "use_project_type", m_cfg_p.m_project_type.m_use_type);
	m_cfg_p.m_project_type.m_type_num = m_iniFile_handle.GetNumber("PROJECT_TYPE", "project_type_num");

	m_cfg_p.m_project_type.m_type_index = 0;
	for (int i = 0; i <= m_cfg_p.m_project_type.m_type_num; i++) {
		sprintf(producer_name, "project_type%d", i);
		m_iniFile_handle.GetString("PROJECT_TYPE", producer_name, m_cfg_p.m_project_type.m_type[i]);

		//printf("%s:%s\n", producer_name, m_cfg_p.m_project_type.m_type[i]);

		// 判断正在使用类型的下标号
		if (strcmp(m_cfg_p.m_project_type.m_use_type, m_cfg_p.m_project_type.m_type[i]) == 0) {
			m_cfg_p.m_project_type.m_type_index = i;
		}
	}

	// 产品类型
	m_iniFile_handle.GetString("PRODUCT_TYPE", "use_product_type", m_cfg_p.m_product_type.m_use_type);
	m_cfg_p.m_product_type.m_type_num = m_iniFile_handle.GetNumber("PRODUCT_TYPE", "product_type_num");

	m_cfg_p.m_product_type.m_type_index = 0;
	for (int i = 0; i <= m_cfg_p.m_product_type.m_type_num; i++) {
		sprintf(producer_name, "product_type%d", i);

		m_iniFile_handle.GetString("PRODUCT_TYPE", producer_name, m_cfg_p.m_product_type.m_type[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_product_type.m_type[i]);

		// 判断正在使用类型的下标号
		if (strcmp(m_cfg_p.m_product_type.m_use_type, m_cfg_p.m_product_type.m_type[i]) == 0) {
			m_cfg_p.m_product_type.m_type_index = i;
		}
	}

	// 4G厂家
	m_cfg_p.m_4g.m_func_sw = m_iniFile_handle.GetNumber("4G", "func_sw");
	m_iniFile_handle.GetString("4G", "use_producer", m_cfg_p.m_4g.m_use_prouducer);
	m_cfg_p.m_4g.m_producer_num = m_iniFile_handle.GetNumber("4G", "producer_num");

	m_cfg_p.m_4g.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_4g.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("4G", producer_name, m_cfg_p.m_4g.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_4g.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_4g.m_use_prouducer, m_cfg_p.m_4g.m_producer[i]) == 0) {
			m_cfg_p.m_4g.m_producer_index = i;
		}
	}

	// GPS厂家
	m_cfg_p.m_gps.m_func_sw = m_iniFile_handle.GetNumber("GPS", "func_sw");
	m_iniFile_handle.GetString("GPS", "use_producer", m_cfg_p.m_gps.m_use_prouducer);
	m_cfg_p.m_gps.m_producer_num = m_iniFile_handle.GetNumber("GPS", "producer_num");

	m_cfg_p.m_gps.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_gps.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("GPS", producer_name, m_cfg_p.m_gps.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_gps.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_gps.m_use_prouducer, m_cfg_p.m_gps.m_producer[i]) == 0) {
			m_cfg_p.m_gps.m_producer_index = i;
		}
	}

	// IMU厂家
	m_cfg_p.m_imu.m_func_sw = m_iniFile_handle.GetNumber("IMU", "func_sw");
	m_iniFile_handle.GetString("IMU", "use_producer", m_cfg_p.m_imu.m_use_prouducer);
	m_cfg_p.m_imu.m_producer_num = m_iniFile_handle.GetNumber("IMU", "producer_num");

	m_cfg_p.m_imu.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_imu.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("IMU", producer_name, m_cfg_p.m_imu.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_imu.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_imu.m_use_prouducer, m_cfg_p.m_imu.m_producer[i]) == 0) {
			m_cfg_p.m_imu.m_producer_index = i;
		}
	}

	// 双目相机厂家
	m_cfg_p.m_dbl_cam.m_func_sw = m_iniFile_handle.GetNumber("DBL_CAM", "func_sw");
	m_iniFile_handle.GetString("DBL_CAM", "use_producer", m_cfg_p.m_dbl_cam.m_use_prouducer);
	m_cfg_p.m_dbl_cam.m_producer_num = m_iniFile_handle.GetNumber("DBL_CAM", "producer_num");

	m_cfg_p.m_dbl_cam.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_dbl_cam.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("DBL_CAM", producer_name, m_cfg_p.m_dbl_cam.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_dbl_cam.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_dbl_cam.m_use_prouducer, m_cfg_p.m_dbl_cam.m_producer[i]) == 0) {
			m_cfg_p.m_dbl_cam.m_producer_index = i;
		}
	}

	// 单目相机厂家
	m_cfg_p.m_sgl_cam.m_func_sw = m_iniFile_handle.GetNumber("SGL_CAM", "func_sw");
	m_iniFile_handle.GetString("SGL_CAM", "use_producer", m_cfg_p.m_sgl_cam.m_use_prouducer);
	m_cfg_p.m_sgl_cam.m_producer_num = m_iniFile_handle.GetNumber("SGL_CAM", "producer_num");

	m_cfg_p.m_sgl_cam.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_sgl_cam.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("SGL_CAM", producer_name, m_cfg_p.m_sgl_cam.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_sgl_cam.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_sgl_cam.m_use_prouducer, m_cfg_p.m_sgl_cam.m_producer[i]) == 0) {
			m_cfg_p.m_sgl_cam.m_producer_index = i;
		}
	}

	// 行车记录仪厂家
	m_cfg_p.m_car_dvr.m_func_sw = m_iniFile_handle.GetNumber("CAR_DVR", "func_sw");
	m_iniFile_handle.GetString("CAR_DVR", "use_producer", m_cfg_p.m_car_dvr.m_use_prouducer);
	m_cfg_p.m_car_dvr.m_producer_num = m_iniFile_handle.GetNumber("CAR_DVR", "producer_num");

	m_cfg_p.m_car_dvr.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_car_dvr.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("CAR_DVR", producer_name, m_cfg_p.m_car_dvr.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_car_dvr.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_car_dvr.m_use_prouducer, m_cfg_p.m_car_dvr.m_producer[i]) == 0) {
			m_cfg_p.m_car_dvr.m_producer_index = i;
		}
	}

	// 刹车制动机构厂家
	m_cfg_p.m_brk_act.m_func_sw = m_iniFile_handle.GetNumber("BRK_ACT", "func_sw");
	m_iniFile_handle.GetString("BRK_ACT", "use_producer", m_cfg_p.m_brk_act.m_use_prouducer);
	m_cfg_p.m_brk_act.m_producer_num = m_iniFile_handle.GetNumber("BRK_ACT", "producer_num");

	m_cfg_p.m_brk_act.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_brk_act.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("BRK_ACT", producer_name, m_cfg_p.m_brk_act.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_brk_act.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_brk_act.m_use_prouducer, m_cfg_p.m_brk_act.m_producer[i]) == 0) {
			m_cfg_p.m_brk_act.m_producer_index = i;
		}
	}

	// 前方超声波雷达厂家
	m_cfg_p.m_ultra_front.m_func_sw = m_iniFile_handle.GetNumber("ULTRA_FRONT", "func_sw");
	m_iniFile_handle.GetString("ULTRA_FRONT", "use_producer", m_cfg_p.m_ultra_front.m_use_prouducer);
	m_cfg_p.m_ultra_front.m_producer_num = m_iniFile_handle.GetNumber("ULTRA_FRONT", "producer_num");

	m_cfg_p.m_ultra_front.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_ultra_front.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("ULTRA_FRONT", producer_name, m_cfg_p.m_ultra_front.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_ultra_front.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_ultra_front.m_use_prouducer, m_cfg_p.m_ultra_front.m_producer[i]) == 0) {
			m_cfg_p.m_ultra_front.m_producer_index = i;
		}
	}
	// 控制器编号
	m_cfg_p.m_ultra_front.controler_id = m_iniFile_handle.GetNumber("ULTRA_FRONT", "controlerID");
	if (m_cfg_p.m_ultra_front.controler_id < 0 || m_cfg_p.m_ultra_front.controler_id > 3) {
		MessageBox(_T("[前]超声波雷达控制器编号范围0~3."), _T("错误"), MB_OK);
	}

	// 单个雷达配置
	m_cfg_p.m_ultra_front.m_rdr1_sw = m_iniFile_handle.GetNumber("ULTRA_FRONT", "rdr1");
	m_cfg_p.m_ultra_front.m_rdr2_sw = m_iniFile_handle.GetNumber("ULTRA_FRONT", "rdr2");
	m_cfg_p.m_ultra_front.m_rdr3_sw = m_iniFile_handle.GetNumber("ULTRA_FRONT", "rdr3");
	m_cfg_p.m_ultra_front.m_rdr4_sw = m_iniFile_handle.GetNumber("ULTRA_FRONT", "rdr4");
	m_cfg_p.m_ultra_front.m_rdr5_sw = m_iniFile_handle.GetNumber("ULTRA_FRONT", "rdr5");
	m_cfg_p.m_ultra_front.m_rdr6_sw = m_iniFile_handle.GetNumber("ULTRA_FRONT", "rdr6");
	m_cfg_p.m_ultra_front.m_sensor_num = 0;
	if (m_cfg_p.m_ultra_front.m_rdr1_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_cfg_p.m_ultra_front.m_rdr2_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_cfg_p.m_ultra_front.m_rdr3_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_cfg_p.m_ultra_front.m_rdr4_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_cfg_p.m_ultra_front.m_rdr5_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_cfg_p.m_ultra_front.m_rdr6_sw) m_cfg_p.m_ultra_front.m_sensor_num++;

	// 后方超声波雷达厂家
	m_cfg_p.m_ultra_end.m_func_sw = m_iniFile_handle.GetNumber("ULTRA_END", "func_sw");
	m_iniFile_handle.GetString("ULTRA_END", "use_producer", m_cfg_p.m_ultra_end.m_use_prouducer);
	m_cfg_p.m_ultra_end.m_producer_num = m_iniFile_handle.GetNumber("ULTRA_END", "producer_num");

	m_cfg_p.m_ultra_end.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_ultra_end.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("ULTRA_END", producer_name, m_cfg_p.m_ultra_end.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_ultra_end.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_ultra_end.m_use_prouducer, m_cfg_p.m_ultra_end.m_producer[i]) == 0) {
			m_cfg_p.m_ultra_end.m_producer_index = i;
		}
	}
	// 控制器编号
	m_cfg_p.m_ultra_end.controler_id = m_iniFile_handle.GetNumber("ULTRA_END", "controlerID");
	if (m_cfg_p.m_ultra_end.controler_id < 0 || m_cfg_p.m_ultra_end.controler_id > 3) {
		MessageBox(_T("[后]超声波雷达控制器编号范围0~3."), _T("错误"), MB_OK);
	}

	// 单个雷达配置
	m_cfg_p.m_ultra_end.m_rdr1_sw = m_iniFile_handle.GetNumber("ULTRA_END", "rdr1");
	m_cfg_p.m_ultra_end.m_rdr2_sw = m_iniFile_handle.GetNumber("ULTRA_END", "rdr2");
	m_cfg_p.m_ultra_end.m_rdr3_sw = m_iniFile_handle.GetNumber("ULTRA_END", "rdr3");
	m_cfg_p.m_ultra_end.m_rdr4_sw = m_iniFile_handle.GetNumber("ULTRA_END", "rdr4");
	m_cfg_p.m_ultra_end.m_rdr5_sw = m_iniFile_handle.GetNumber("ULTRA_END", "rdr5");
	m_cfg_p.m_ultra_end.m_rdr6_sw = m_iniFile_handle.GetNumber("ULTRA_END", "rdr6");
	m_cfg_p.m_ultra_end.m_sensor_num = 0;
	if (m_cfg_p.m_ultra_end.m_rdr1_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_cfg_p.m_ultra_end.m_rdr2_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_cfg_p.m_ultra_end.m_rdr3_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_cfg_p.m_ultra_end.m_rdr4_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_cfg_p.m_ultra_end.m_rdr5_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_cfg_p.m_ultra_end.m_rdr6_sw) m_cfg_p.m_ultra_end.m_sensor_num++;

	// 左侧超声波雷达厂家
	m_cfg_p.m_ultra_left.m_func_sw = m_iniFile_handle.GetNumber("ULTRA_LEFT", "func_sw");
	m_iniFile_handle.GetString("ULTRA_LEFT", "use_producer", m_cfg_p.m_ultra_left.m_use_prouducer);
	m_cfg_p.m_ultra_left.m_producer_num = m_iniFile_handle.GetNumber("ULTRA_LEFT", "producer_num");

	m_cfg_p.m_ultra_left.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_ultra_left.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("ULTRA_LEFT", producer_name, m_cfg_p.m_ultra_left.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_ultra_left.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_ultra_left.m_use_prouducer, m_cfg_p.m_ultra_left.m_producer[i]) == 0) {
			m_cfg_p.m_ultra_left.m_producer_index = i;
		}
	}
	// 控制器编号
	m_cfg_p.m_ultra_left.controler_id = m_iniFile_handle.GetNumber("ULTRA_LEFT", "controlerID");
	if (m_cfg_p.m_ultra_left.controler_id < 0 || m_cfg_p.m_ultra_left.controler_id > 3) {
		MessageBox(_T("[左]超声波雷达控制器编号范围0~3."), _T("错误"), MB_OK);
	}

	// 单个雷达配置
	m_cfg_p.m_ultra_left.m_rdr1_sw = m_iniFile_handle.GetNumber("ULTRA_LEFT", "rdr1");
	m_cfg_p.m_ultra_left.m_rdr2_sw = m_iniFile_handle.GetNumber("ULTRA_LEFT", "rdr2");
	m_cfg_p.m_ultra_left.m_rdr3_sw = m_iniFile_handle.GetNumber("ULTRA_LEFT", "rdr3");
	m_cfg_p.m_ultra_left.m_rdr4_sw = m_iniFile_handle.GetNumber("ULTRA_LEFT", "rdr4");
	m_cfg_p.m_ultra_left.m_rdr5_sw = m_iniFile_handle.GetNumber("ULTRA_LEFT", "rdr5");
	m_cfg_p.m_ultra_left.m_rdr6_sw = m_iniFile_handle.GetNumber("ULTRA_LEFT", "rdr6");
	m_cfg_p.m_ultra_left.m_sensor_num = 0;
	if (m_cfg_p.m_ultra_left.m_rdr1_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_cfg_p.m_ultra_left.m_rdr2_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_cfg_p.m_ultra_left.m_rdr3_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_cfg_p.m_ultra_left.m_rdr4_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_cfg_p.m_ultra_left.m_rdr5_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_cfg_p.m_ultra_left.m_rdr6_sw) m_cfg_p.m_ultra_left.m_sensor_num++;

	// 右侧超声波雷达厂家
	m_cfg_p.m_ultra_right.m_func_sw = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "func_sw");
	m_iniFile_handle.GetString("ULTRA_RIGHT", "use_producer", m_cfg_p.m_ultra_right.m_use_prouducer);
	m_cfg_p.m_ultra_right.m_producer_num = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "producer_num");

	m_cfg_p.m_ultra_right.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_ultra_right.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("ULTRA_RIGHT", producer_name, m_cfg_p.m_ultra_right.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_ultra_right.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_ultra_right.m_use_prouducer, m_cfg_p.m_ultra_right.m_producer[i]) == 0) {
			m_cfg_p.m_ultra_right.m_producer_index = i;
		}
	}
	// 控制器编号
	m_cfg_p.m_ultra_right.controler_id = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "controlerID");
	if (m_cfg_p.m_ultra_right.controler_id < 0 || m_cfg_p.m_ultra_right.controler_id > 3) {
		MessageBox(_T("[右]超声波雷达控制器编号范围]0~3."), _T("错误"), MB_OK);
	}

	// 单个雷达配置
	m_cfg_p.m_ultra_right.m_rdr1_sw = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "rdr1");
	m_cfg_p.m_ultra_right.m_rdr2_sw = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "rdr2");
	m_cfg_p.m_ultra_right.m_rdr3_sw = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "rdr3");
	m_cfg_p.m_ultra_right.m_rdr4_sw = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "rdr4");
	m_cfg_p.m_ultra_right.m_rdr5_sw = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "rdr5");
	m_cfg_p.m_ultra_right.m_rdr6_sw = m_iniFile_handle.GetNumber("ULTRA_RIGHT", "rdr6");
	m_cfg_p.m_ultra_right.m_sensor_num = 0;
	if (m_cfg_p.m_ultra_right.m_rdr1_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_cfg_p.m_ultra_right.m_rdr2_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_cfg_p.m_ultra_right.m_rdr3_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_cfg_p.m_ultra_right.m_rdr4_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_cfg_p.m_ultra_right.m_rdr5_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_cfg_p.m_ultra_right.m_rdr6_sw) m_cfg_p.m_ultra_right.m_sensor_num++;

	// 前方毫米波雷达厂家
	m_cfg_p.m_mmw_front.m_func_sw = m_iniFile_handle.GetNumber("MMW_FRONT", "func_sw");
	m_iniFile_handle.GetString("MMW_FRONT", "use_producer", m_cfg_p.m_mmw_front.m_use_prouducer);
	m_cfg_p.m_mmw_front.m_producer_num = m_iniFile_handle.GetNumber("MMW_FRONT", "producer_num");

	m_cfg_p.m_mmw_front.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_mmw_front.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("MMW_FRONT", producer_name, m_cfg_p.m_mmw_front.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_mmw_front.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_mmw_front.m_use_prouducer, m_cfg_p.m_mmw_front.m_producer[i]) == 0) {
			m_cfg_p.m_mmw_front.m_producer_index = i;
		}
	}
	// 单个雷达配置
	m_cfg_p.m_mmw_front.m_rdr1_sw = m_iniFile_handle.GetNumber("MMW_FRONT", "rdr1");
	m_cfg_p.m_mmw_front.m_sensor_num = 0;
	if (m_cfg_p.m_mmw_front.m_rdr1_sw) m_cfg_p.m_mmw_front.m_sensor_num++;

	// 后方毫米波雷达厂家
	m_cfg_p.m_mmw_end.m_func_sw = m_iniFile_handle.GetNumber("MMW_END", "func_sw");
	m_iniFile_handle.GetString("MMW_END", "use_producer", m_cfg_p.m_mmw_end.m_use_prouducer);
	m_cfg_p.m_mmw_end.m_producer_num = m_iniFile_handle.GetNumber("MMW_END", "producer_num");

	m_cfg_p.m_mmw_end.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_mmw_end.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("MMW_END", producer_name, m_cfg_p.m_mmw_end.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_mmw_end.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_mmw_end.m_use_prouducer, m_cfg_p.m_mmw_end.m_producer[i]) == 0) {
			m_cfg_p.m_mmw_end.m_producer_index = i;
		}
	}
	// 单个雷达配置
	m_cfg_p.m_mmw_end.m_rdr1_sw = m_iniFile_handle.GetNumber("MMW_END", "rdr1");
	m_cfg_p.m_mmw_end.m_sensor_num = 0;
	if (m_cfg_p.m_mmw_end.m_rdr1_sw) m_cfg_p.m_mmw_end.m_sensor_num++;

	// 左侧毫米波雷达厂家
	m_cfg_p.m_mmw_left.m_func_sw = m_iniFile_handle.GetNumber("MMW_LEFT", "func_sw");
	m_iniFile_handle.GetString("MMW_LEFT", "use_producer", m_cfg_p.m_mmw_left.m_use_prouducer);
	m_cfg_p.m_mmw_left.m_producer_num = m_iniFile_handle.GetNumber("MMW_LEFT", "producer_num");

	m_cfg_p.m_mmw_left.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_mmw_left.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("MMW_LEFT", producer_name, m_cfg_p.m_mmw_left.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_mmw_left.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_mmw_left.m_use_prouducer, m_cfg_p.m_mmw_left.m_producer[i]) == 0) {
			m_cfg_p.m_mmw_left.m_producer_index = i;
		}
	}
	// 单个雷达配置
	m_cfg_p.m_mmw_left.m_rdr1_sw = m_iniFile_handle.GetNumber("MMW_LEFT", "rdr1");
	m_cfg_p.m_mmw_left.m_rdr2_sw = m_iniFile_handle.GetNumber("MMW_LEFT", "rdr2");
	m_cfg_p.m_mmw_left.m_sensor_num = 0;
	if (m_cfg_p.m_mmw_left.m_rdr1_sw) m_cfg_p.m_mmw_left.m_sensor_num++;
	if (m_cfg_p.m_mmw_left.m_rdr1_sw) m_cfg_p.m_mmw_left.m_sensor_num++;

	// 右侧毫米波雷达厂家
	m_cfg_p.m_mmw_right.m_func_sw = m_iniFile_handle.GetNumber("MMW_RIGHT", "func_sw");
	m_iniFile_handle.GetString("MMW_RIGHT", "use_producer", m_cfg_p.m_mmw_right.m_use_prouducer);
	m_cfg_p.m_mmw_right.m_producer_num = m_iniFile_handle.GetNumber("MMW_RIGHT", "producer_num");

	m_cfg_p.m_mmw_right.m_producer_index = 0;
	for (int i = 0; i <= m_cfg_p.m_mmw_right.m_producer_num; i++) {
		sprintf(producer_name, "producer%d", i);

		m_iniFile_handle.GetString("MMW_RIGHT", producer_name, m_cfg_p.m_mmw_right.m_producer[i]);
		//printf("%s:%s\n", producer_name, m_cfg_p.m_mmw_right.m_producer[i]);

		// 判断正在使用厂家的下标号
		if (strcmp(m_cfg_p.m_mmw_right.m_use_prouducer, m_cfg_p.m_mmw_right.m_producer[i]) == 0) {
			m_cfg_p.m_mmw_right.m_producer_index = i;
		}
	}
	// 单个雷达配置
	m_cfg_p.m_mmw_right.m_rdr1_sw = m_iniFile_handle.GetNumber("MMW_RIGHT", "rdr1");
	m_cfg_p.m_mmw_right.m_rdr2_sw = m_iniFile_handle.GetNumber("MMW_RIGHT", "rdr2");
	m_cfg_p.m_mmw_right.m_sensor_num = 0;
	if (m_cfg_p.m_mmw_right.m_rdr1_sw) m_cfg_p.m_mmw_right.m_sensor_num++;
	if (m_cfg_p.m_mmw_right.m_rdr1_sw) m_cfg_p.m_mmw_right.m_sensor_num++;

	return true;
}
bool CSensorParamHexDlg::Load_Config_Param_Ini_File(Config_Param * cfg_p)
{
	return Read_Ini_Config_File();
}

void CSensorParamHexDlg::Debug_Print_Cfg_Param_Info(Config_Param cfg_p)
{

}

void CSensorParamHexDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialogEx::OnTimer(nIDEvent);
	switch (nIDEvent) {
	case 2: {
		//CDC* pDC = GetDlgItem(IDC_STA_ULTRA_PIC)->GetDC();//获得pictrue控件的DC  
		//image.Draw(pDC->m_hDC, rect); //将图片画到Picture控件表示的矩形区域
		//ReleaseDC(pDC);//释放picture控件的DC
	}break;
	}
}

void CSensorParamHexDlg::OnBnClickedBtn4gStart()
{
	if (!m_4g_cfg.m_func_btn_sw) {
		m_4g_cfg.m_func_btn_sw = true;
		// 显示属性
		m_4g_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_4G_Maker)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_4g_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_4g_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_4g_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_4G_Maker)->ShowWindow(SW_HIDE);
		m_4g_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnGpsStart()
{
	if (!m_gps_cfg.m_func_btn_sw) {
		m_gps_cfg.m_func_btn_sw = true;
		// 显示属性
		//GetDlgItem(IDC_BTN_GPS_Start)->SetWindowText("功能开启");
		//GetDlgItem(IDC_COMB_GPS_Maker)->ShowWindow(SW_SHOW);

		m_gps_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_GPS_Maker)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_gps_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_gps_cfg.m_func_btn_sw = false;
		//// 隐藏属性
		//GetDlgItem(IDC_BTN_GPS_Start)->SetWindowText("功能关闭");
		//GetDlgItem(IDC_COMB_GPS_Maker)->ShowWindow(SW_HIDE);

		m_gps_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_GPS_Maker)->ShowWindow(SW_HIDE);
		m_gps_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnImuStart()
{
	if (!m_imu_cfg.m_func_btn_sw) {
		m_imu_cfg.m_func_btn_sw = true;
		// 显示属性
		m_imu_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_IMU_Maker)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_imu_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_imu_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_imu_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_IMU_Maker)->ShowWindow(SW_HIDE);
		m_imu_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnDblCameraStart()
{
	if (!m_dbl_cam_cfg.m_func_btn_sw) {
		m_dbl_cam_cfg.m_func_btn_sw = true;
		// 显示属性
		m_dbl_cam_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_Dbl_Camera_Maker)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_dbl_cam_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_dbl_cam_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_dbl_cam_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_Dbl_Camera_Maker)->ShowWindow(SW_HIDE);
		m_dbl_cam_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnSglCameraStart()
{
	if (!m_sgl_cam_cfg.m_func_btn_sw) {
		m_sgl_cam_cfg.m_func_btn_sw = true;
		// 显示属性
		m_sgl_cam_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_Sgl_Camera_Maker)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_sgl_cam_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_sgl_cam_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_sgl_cam_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_Sgl_Camera_Maker)->ShowWindow(SW_HIDE);
		m_sgl_cam_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnCarDvrStart()
{
	if (!m_car_dvr_cfg.m_func_btn_sw) {
		m_car_dvr_cfg.m_func_btn_sw = true;
		// 显示属性
		m_car_dvr_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_Car_DVR_Maker)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_car_dvr_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_car_dvr_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_car_dvr_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_Car_DVR_Maker)->ShowWindow(SW_HIDE);
		m_car_dvr_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnBrkActStart()
{
	if (!m_brk_act_cfg.m_func_btn_sw) {
		m_brk_act_cfg.m_func_btn_sw = true;
		// 显示属性
		m_brk_act_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_BRK_Act_Maker)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_brk_act_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_brk_act_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_brk_act_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_BRK_Act_Maker)->ShowWindow(SW_HIDE);
		m_brk_act_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnUltraFrontStart()
{
	if (!m_ultra_front_cfg.m_func_btn_sw) {
		m_ultra_front_cfg.m_func_btn_sw = true;
		// 显示属性
		m_ultra_front_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_Ultra_Front_Maker)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Front_1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Front_2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Front_3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Front_4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Front_5)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Front_6)->ShowWindow(SW_SHOW);
		m_ultra_front_cfg.m_ctrler_comb_handle.ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色i
		m_ultra_front_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_ultra_front_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_ultra_front_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_Ultra_Front_Maker)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Front_1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Front_2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Front_3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Front_4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Front_5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Front_6)->ShowWindow(SW_HIDE);
		m_ultra_front_cfg.m_ctrler_comb_handle.ShowWindow(SW_HIDE);
		m_ultra_front_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnUltraEndStart()
{
	if (!m_ultra_end_cfg.m_func_btn_sw) {
		m_ultra_end_cfg.m_func_btn_sw = true;
		// 显示属性
		m_ultra_end_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_Ultra_End_Maker)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_End_1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_End_2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_End_3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_End_4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_End_5)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_End_6)->ShowWindow(SW_SHOW);
		m_ultra_end_cfg.m_ctrler_comb_handle.ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_ultra_end_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_ultra_end_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_ultra_end_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_Ultra_End_Maker)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_End_1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_End_2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_End_3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_End_4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_End_5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_End_6)->ShowWindow(SW_HIDE);
		m_ultra_end_cfg.m_ctrler_comb_handle.ShowWindow(SW_HIDE);
		m_ultra_end_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnUltraLeftStart()
{
	if (!m_ultra_left_cfg.m_func_btn_sw) {
		m_ultra_left_cfg.m_func_btn_sw = true;
		// 显示属性
		m_ultra_left_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_Ultra_Left_Maker)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Left_1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Left_2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Left_3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Left_4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Left_5)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Left_6)->ShowWindow(SW_SHOW);
		m_ultra_left_cfg.m_ctrler_comb_handle.ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_ultra_left_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_ultra_left_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_ultra_left_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_Ultra_Left_Maker)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Left_1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Left_2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Left_3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Left_4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Left_5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Left_6)->ShowWindow(SW_HIDE);
		m_ultra_left_cfg.m_ctrler_comb_handle.ShowWindow(SW_HIDE);
		m_ultra_left_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnUltraRightStart()
{
	if (!m_ultra_right_cfg.m_func_btn_sw) {
		m_ultra_right_cfg.m_func_btn_sw = true;
		// 显示属性
		m_ultra_right_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_Ultra_Right_Maker)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Right_1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Right_2)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Right_3)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Right_4)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Right_5)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_Ultra_Right_6)->ShowWindow(SW_SHOW);
		m_ultra_right_cfg.m_ctrler_comb_handle.ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_ultra_right_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_ultra_right_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_ultra_right_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_Ultra_Right_Maker)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Right_1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Right_2)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Right_3)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Right_4)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Right_5)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_Ultra_Right_6)->ShowWindow(SW_HIDE);
		m_ultra_right_cfg.m_ctrler_comb_handle.ShowWindow(SW_HIDE);
		m_ultra_right_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwFrontStart()
{
	if (!m_mmw_front_cfg.m_func_btn_sw) {
		m_mmw_front_cfg.m_func_btn_sw = true;
		// 显示属性
		m_mmw_front_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_MMW_Front_Maker)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_MMW_Front_1)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_mmw_front_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_mmw_front_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_mmw_front_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_MMW_Front_Maker)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_MMW_Front_1)->ShowWindow(SW_HIDE);
		m_mmw_front_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwEndStart()
{
	if (!m_mmw_end_cfg.m_func_btn_sw) {
		m_mmw_end_cfg.m_func_btn_sw = true;
		// 显示属性
		m_mmw_end_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_MMW_End_Maker)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_MMW_End_1)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_mmw_end_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_mmw_end_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_mmw_end_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_MMW_End_Maker)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_MMW_End_1)->ShowWindow(SW_HIDE);
		m_mmw_end_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwLeftStart()
{
	if (!m_mmw_left_cfg.m_func_btn_sw) {
		m_mmw_left_cfg.m_func_btn_sw = true;
		// 显示属性
		m_mmw_left_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_MMW_Left_Maker)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_MMW_Left_1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_MMW_Left_2)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_mmw_left_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_mmw_left_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_mmw_left_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_MMW_Left_Maker)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_MMW_Left_1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_MMW_Left_2)->ShowWindow(SW_HIDE);
		m_mmw_left_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwRightStart()
{
	if (!m_mmw_right_cfg.m_func_btn_sw) {
		m_mmw_right_cfg.m_func_btn_sw = true;
		// 显示属性
		m_mmw_right_cfg.m_func_btn_handle.SetWindowText("功能开启");
		GetDlgItem(IDC_COMB_MMW_Right_Maker)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_MMW_Right_1)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_BTN_MMW_Right_2)->ShowWindow(SW_SHOW);
		// 按钮颜色改成绿色
		m_mmw_right_cfg.m_func_btn_handle.SetFaceColor(ENABLE_COLOUR);
	}
	else {
		m_mmw_right_cfg.m_func_btn_sw = false;
		// 隐藏属性
		m_mmw_right_cfg.m_func_btn_handle.SetWindowText("功能关闭");
		GetDlgItem(IDC_COMB_MMW_Right_Maker)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_MMW_Right_1)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BTN_MMW_Right_2)->ShowWindow(SW_HIDE);
		m_mmw_right_cfg.m_func_btn_handle.SetFaceColor(DISABLE_COLOUR);
	}
}

void CSensorParamHexDlg::OnBnClickedBtnUltraFront1()
{
	if (!m_ultra_front_cfg.m_rdr1_btn_sw) {
		m_ultra_front_cfg.m_rdr1_btn_sw = true;
		// 显示属性
		m_ultra_front_cfg.m_rdr1_btn_handle.SetWindowText("1号有");
		// 按钮颜色改成绿色
		m_ultra_front_cfg.m_rdr1_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_front_cfg.rdr_num++;
		//if (m_ultra_front_cfg.rdr_num > 6) m_ultra_front_cfg.rdr_num = 6;
	}
	else {
		m_ultra_front_cfg.m_rdr1_btn_sw = false;
		// 隐藏属性
		m_ultra_front_cfg.m_rdr1_btn_handle.SetWindowText("1号无");
		m_ultra_front_cfg.m_rdr1_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_front_cfg.rdr_num--;
		//if (m_ultra_front_cfg.rdr_num < 0) m_ultra_front_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_front_cfg.rdr_num);
	//m_ultra_front_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraFront2()
{
	if (!m_ultra_front_cfg.m_rdr2_btn_sw) {
		m_ultra_front_cfg.m_rdr2_btn_sw = true;
		// 显示属性
		m_ultra_front_cfg.m_rdr2_btn_handle.SetWindowText("2号有");
		// 按钮颜色改成绿色
		m_ultra_front_cfg.m_rdr2_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_front_cfg.rdr_num++;
		//if (m_ultra_front_cfg.rdr_num > 6) m_ultra_front_cfg.rdr_num = 6;
	}
	else {
		m_ultra_front_cfg.m_rdr2_btn_sw = false;
		// 隐藏属性
		m_ultra_front_cfg.m_rdr2_btn_handle.SetWindowText("2号无");
		m_ultra_front_cfg.m_rdr2_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_front_cfg.rdr_num--;
		//if (m_ultra_front_cfg.rdr_num < 0) m_ultra_front_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_front_cfg.rdr_num);
	//m_ultra_front_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraFront3()
{
	if (!m_ultra_front_cfg.m_rdr3_btn_sw) {
		m_ultra_front_cfg.m_rdr3_btn_sw = true;
		// 显示属性
		m_ultra_front_cfg.m_rdr3_btn_handle.SetWindowText("3号有");
		// 按钮颜色改成绿色
		m_ultra_front_cfg.m_rdr3_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_front_cfg.rdr_num++;
		//if (m_ultra_front_cfg.rdr_num > 6) m_ultra_front_cfg.rdr_num = 6;
	}
	else {
		m_ultra_front_cfg.m_rdr3_btn_sw = false;
		// 隐藏属性
		m_ultra_front_cfg.m_rdr3_btn_handle.SetWindowText("3号无");
		m_ultra_front_cfg.m_rdr3_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_front_cfg.rdr_num--;
		//if (m_ultra_front_cfg.rdr_num < 0) m_ultra_front_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_front_cfg.rdr_num);
	//m_ultra_front_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraFront4()
{
	if (!m_ultra_front_cfg.m_rdr4_btn_sw) {
		m_ultra_front_cfg.m_rdr4_btn_sw = true;
		// 显示属性
		m_ultra_front_cfg.m_rdr4_btn_handle.SetWindowText("4号有");
		// 按钮颜色改成绿色
		m_ultra_front_cfg.m_rdr4_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_front_cfg.rdr_num++;
		//if (m_ultra_front_cfg.rdr_num > 6) m_ultra_front_cfg.rdr_num = 6;
	}
	else {
		m_ultra_front_cfg.m_rdr4_btn_sw = false;
		// 隐藏属性
		m_ultra_front_cfg.m_rdr4_btn_handle.SetWindowText("4号无");
		m_ultra_front_cfg.m_rdr4_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_front_cfg.rdr_num--;
		//if (m_ultra_front_cfg.rdr_num < 0) m_ultra_front_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_front_cfg.rdr_num);
	//m_ultra_front_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraFront5()
{
	if (!m_ultra_front_cfg.m_rdr5_btn_sw) {
		m_ultra_front_cfg.m_rdr5_btn_sw = true;
		// 显示属性
		m_ultra_front_cfg.m_rdr5_btn_handle.SetWindowText("5号有");
		// 按钮颜色改成绿色
		m_ultra_front_cfg.m_rdr5_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_front_cfg.rdr_num++;
		//if (m_ultra_front_cfg.rdr_num > 6) m_ultra_front_cfg.rdr_num = 6;
	}
	else {
		m_ultra_front_cfg.m_rdr5_btn_sw = false;
		// 隐藏属性
		m_ultra_front_cfg.m_rdr5_btn_handle.SetWindowText("5号无");
		m_ultra_front_cfg.m_rdr5_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_front_cfg.rdr_num--;
		//if (m_ultra_front_cfg.rdr_num < 0) m_ultra_front_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_front_cfg.rdr_num);
	//m_ultra_front_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraFront6()
{
	if (!m_ultra_front_cfg.m_rdr6_btn_sw) {
		m_ultra_front_cfg.m_rdr6_btn_sw = true;
		// 显示属性
		m_ultra_front_cfg.m_rdr6_btn_handle.SetWindowText("6号有");
		// 按钮颜色改成绿色
		m_ultra_front_cfg.m_rdr6_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_front_cfg.rdr_num++;
		//if (m_ultra_front_cfg.rdr_num > 6) m_ultra_front_cfg.rdr_num = 6;
	}
	else {
		m_ultra_front_cfg.m_rdr6_btn_sw = false;
		// 隐藏属性
		m_ultra_front_cfg.m_rdr6_btn_handle.SetWindowText("6号无");
		m_ultra_front_cfg.m_rdr6_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_front_cfg.rdr_num--;
		//if (m_ultra_front_cfg.rdr_num < 0) m_ultra_front_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_front_cfg.rdr_num);
	//m_ultra_front_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraEnd1()
{
	if (!m_ultra_end_cfg.m_rdr1_btn_sw) {
		m_ultra_end_cfg.m_rdr1_btn_sw = true;
		// 显示属性
		m_ultra_end_cfg.m_rdr1_btn_handle.SetWindowText("1号有");
		// 按钮颜色改成绿色
		m_ultra_end_cfg.m_rdr1_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_end_cfg.rdr_num++;
		//if (m_ultra_end_cfg.rdr_num > 6) m_ultra_end_cfg.rdr_num = 6;
	}
	else {
		m_ultra_end_cfg.m_rdr1_btn_sw = false;
		// 隐藏属性
		m_ultra_end_cfg.m_rdr1_btn_handle.SetWindowText("1号无");
		m_ultra_end_cfg.m_rdr1_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_end_cfg.rdr_num--;
		//if (m_ultra_end_cfg.rdr_num < 0) m_ultra_end_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_end_cfg.rdr_num);
	//m_ultra_end_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraEnd2()
{
	if (!m_ultra_end_cfg.m_rdr2_btn_sw) {
		m_ultra_end_cfg.m_rdr2_btn_sw = true;
		// 显示属性
		m_ultra_end_cfg.m_rdr2_btn_handle.SetWindowText("2号有");
		// 按钮颜色改成绿色
		m_ultra_end_cfg.m_rdr2_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_end_cfg.rdr_num++;
		//if (m_ultra_end_cfg.rdr_num > 6) m_ultra_end_cfg.rdr_num = 6;
	}
	else {
		m_ultra_end_cfg.m_rdr2_btn_sw = false;
		// 隐藏属性
		m_ultra_end_cfg.m_rdr2_btn_handle.SetWindowText("2号无");
		m_ultra_end_cfg.m_rdr2_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_end_cfg.rdr_num--;
		//if (m_ultra_end_cfg.rdr_num < 0) m_ultra_end_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_end_cfg.rdr_num);
	//m_ultra_end_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraEnd3()
{
	if (!m_ultra_end_cfg.m_rdr3_btn_sw) {
		m_ultra_end_cfg.m_rdr3_btn_sw = true;
		// 显示属性
		m_ultra_end_cfg.m_rdr3_btn_handle.SetWindowText("3号有");
		// 按钮颜色改成绿色
		m_ultra_end_cfg.m_rdr3_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_end_cfg.rdr_num++;
		//if (m_ultra_end_cfg.rdr_num > 6) m_ultra_end_cfg.rdr_num = 6;
	}
	else {
		m_ultra_end_cfg.m_rdr3_btn_sw = false;
		// 隐藏属性
		m_ultra_end_cfg.m_rdr3_btn_handle.SetWindowText("3号无");
		m_ultra_end_cfg.m_rdr3_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_end_cfg.rdr_num--;
		//if (m_ultra_end_cfg.rdr_num < 0) m_ultra_end_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_end_cfg.rdr_num);
	//m_ultra_end_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraEnd4()
{
	if (!m_ultra_end_cfg.m_rdr4_btn_sw) {
		m_ultra_end_cfg.m_rdr4_btn_sw = true;
		// 显示属性
		m_ultra_end_cfg.m_rdr4_btn_handle.SetWindowText("4号有");
		// 按钮颜色改成绿色
		m_ultra_end_cfg.m_rdr4_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_end_cfg.rdr_num++;
		//if (m_ultra_end_cfg.rdr_num > 6) m_ultra_end_cfg.rdr_num = 6;
	}
	else {
		m_ultra_end_cfg.m_rdr4_btn_sw = false;
		// 隐藏属性
		m_ultra_end_cfg.m_rdr4_btn_handle.SetWindowText("4号无");
		m_ultra_end_cfg.m_rdr4_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_end_cfg.rdr_num--;
		//if (m_ultra_end_cfg.rdr_num < 0) m_ultra_end_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_end_cfg.rdr_num);
	//m_ultra_end_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraEnd5()
{
	if (!m_ultra_end_cfg.m_rdr5_btn_sw) {
		m_ultra_end_cfg.m_rdr5_btn_sw = true;
		// 显示属性
		m_ultra_end_cfg.m_rdr5_btn_handle.SetWindowText("5号有");
		// 按钮颜色改成绿色
		m_ultra_end_cfg.m_rdr5_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_end_cfg.rdr_num++;
		//if (m_ultra_end_cfg.rdr_num > 6) m_ultra_end_cfg.rdr_num = 6;
	}
	else {
		m_ultra_end_cfg.m_rdr5_btn_sw = false;
		// 隐藏属性
		m_ultra_end_cfg.m_rdr5_btn_handle.SetWindowText("5号无");
		m_ultra_end_cfg.m_rdr5_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_end_cfg.rdr_num--;
		//if (m_ultra_end_cfg.rdr_num < 0) m_ultra_end_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_end_cfg.rdr_num);
	//m_ultra_end_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraEnd6()
{
	if (!m_ultra_end_cfg.m_rdr6_btn_sw) {
		m_ultra_end_cfg.m_rdr6_btn_sw = true;
		// 显示属性
		m_ultra_end_cfg.m_rdr6_btn_handle.SetWindowText("6号有");
		// 按钮颜色改成绿色
		m_ultra_end_cfg.m_rdr6_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_end_cfg.rdr_num++;
		//if (m_ultra_end_cfg.rdr_num > 6) m_ultra_end_cfg.rdr_num = 6;
	}
	else {
		m_ultra_end_cfg.m_rdr6_btn_sw = false;
		// 隐藏属性
		m_ultra_end_cfg.m_rdr6_btn_handle.SetWindowText("6号无");
		m_ultra_end_cfg.m_rdr6_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_end_cfg.rdr_num--;
		//if (m_ultra_end_cfg.rdr_num < 0) m_ultra_end_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_end_cfg.rdr_num);
	//m_ultra_end_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraLeft1()
{
	if (!m_ultra_left_cfg.m_rdr1_btn_sw) {
		m_ultra_left_cfg.m_rdr1_btn_sw = true;
		// 显示属性
		m_ultra_left_cfg.m_rdr1_btn_handle.SetWindowText("1号有");
		// 按钮颜色改成绿色
		m_ultra_left_cfg.m_rdr1_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_left_cfg.rdr_num++;
		//if (m_ultra_left_cfg.rdr_num > 6) m_ultra_left_cfg.rdr_num = 6;
	}
	else {
		m_ultra_left_cfg.m_rdr1_btn_sw = false;
		// 隐藏属性
		m_ultra_left_cfg.m_rdr1_btn_handle.SetWindowText("1号无");
		m_ultra_left_cfg.m_rdr1_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_left_cfg.rdr_num--;
		//if (m_ultra_left_cfg.rdr_num < 0) m_ultra_left_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_left_cfg.rdr_num);
	//m_ultra_left_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraLeft2()
{
	if (!m_ultra_left_cfg.m_rdr2_btn_sw) {
		m_ultra_left_cfg.m_rdr2_btn_sw = true;
		// 显示属性
		m_ultra_left_cfg.m_rdr2_btn_handle.SetWindowText("2号有");
		// 按钮颜色改成绿色
		m_ultra_left_cfg.m_rdr2_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_left_cfg.rdr_num++;
		//if (m_ultra_left_cfg.rdr_num > 6) m_ultra_left_cfg.rdr_num = 6;
	}
	else {
		m_ultra_left_cfg.m_rdr2_btn_sw = false;
		// 隐藏属性
		m_ultra_left_cfg.m_rdr2_btn_handle.SetWindowText("2号无");
		m_ultra_left_cfg.m_rdr2_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_left_cfg.rdr_num--;
		//if (m_ultra_left_cfg.rdr_num < 0) m_ultra_left_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_left_cfg.rdr_num);
	//m_ultra_left_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraLeft3()
{
	if (!m_ultra_left_cfg.m_rdr3_btn_sw) {
		m_ultra_left_cfg.m_rdr3_btn_sw = true;
		// 显示属性
		m_ultra_left_cfg.m_rdr3_btn_handle.SetWindowText("3号有");
		// 按钮颜色改成绿色
		m_ultra_left_cfg.m_rdr3_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_left_cfg.rdr_num++;
		//if (m_ultra_left_cfg.rdr_num > 6) m_ultra_left_cfg.rdr_num = 6;
	}
	else {
		m_ultra_left_cfg.m_rdr3_btn_sw = false;
		// 隐藏属性
		m_ultra_left_cfg.m_rdr3_btn_handle.SetWindowText("3号无");
		m_ultra_left_cfg.m_rdr3_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_left_cfg.rdr_num--;
		//if (m_ultra_left_cfg.rdr_num < 0) m_ultra_left_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_left_cfg.rdr_num);
	//m_ultra_left_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraLeft4()
{
	if (!m_ultra_left_cfg.m_rdr4_btn_sw) {
		m_ultra_left_cfg.m_rdr4_btn_sw = true;
		// 显示属性
		m_ultra_left_cfg.m_rdr4_btn_handle.SetWindowText("4号有");
		// 按钮颜色改成绿色
		m_ultra_left_cfg.m_rdr4_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_left_cfg.rdr_num++;
		//if (m_ultra_left_cfg.rdr_num > 6) m_ultra_left_cfg.rdr_num = 6;
	}
	else {
		m_ultra_left_cfg.m_rdr4_btn_sw = false;
		// 隐藏属性
		m_ultra_left_cfg.m_rdr4_btn_handle.SetWindowText("4号无");
		m_ultra_left_cfg.m_rdr4_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_left_cfg.rdr_num--;
		//if (m_ultra_left_cfg.rdr_num < 0) m_ultra_left_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_left_cfg.rdr_num);
	//m_ultra_left_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraLeft5()
{
	if (!m_ultra_left_cfg.m_rdr5_btn_sw) {
		m_ultra_left_cfg.m_rdr5_btn_sw = true;
		// 显示属性
		m_ultra_left_cfg.m_rdr5_btn_handle.SetWindowText("5号有");
		// 按钮颜色改成绿色
		m_ultra_left_cfg.m_rdr5_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_left_cfg.rdr_num++;
		//if (m_ultra_left_cfg.rdr_num > 6) m_ultra_left_cfg.rdr_num = 6;
	}
	else {
		m_ultra_left_cfg.m_rdr5_btn_sw = false;
		// 隐藏属性
		m_ultra_left_cfg.m_rdr5_btn_handle.SetWindowText("5号无");
		m_ultra_left_cfg.m_rdr5_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_left_cfg.rdr_num--;
		//if (m_ultra_left_cfg.rdr_num < 0) m_ultra_left_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_left_cfg.rdr_num);
	//m_ultra_left_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraLeft6()
{
	if (!m_ultra_left_cfg.m_rdr6_btn_sw) {
		m_ultra_left_cfg.m_rdr6_btn_sw = true;
		// 显示属性
		m_ultra_left_cfg.m_rdr6_btn_handle.SetWindowText("6号有");
		// 按钮颜色改成绿色
		m_ultra_left_cfg.m_rdr6_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_left_cfg.rdr_num++;
		//if (m_ultra_left_cfg.rdr_num > 6) m_ultra_left_cfg.rdr_num = 6;
	}
	else {
		m_ultra_left_cfg.m_rdr6_btn_sw = false;
		// 隐藏属性
		m_ultra_left_cfg.m_rdr6_btn_handle.SetWindowText("6号无");
		m_ultra_left_cfg.m_rdr6_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_left_cfg.rdr_num--;
		//if (m_ultra_left_cfg.rdr_num < 0) m_ultra_left_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_left_cfg.rdr_num);
	//m_ultra_left_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraRight1()
{
	if (!m_ultra_right_cfg.m_rdr1_btn_sw) {
		m_ultra_right_cfg.m_rdr1_btn_sw = true;
		// 显示属性
		m_ultra_right_cfg.m_rdr1_btn_handle.SetWindowText("1号有");
		// 按钮颜色改成绿色
		m_ultra_right_cfg.m_rdr1_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_right_cfg.rdr_num++;
		//if (m_ultra_right_cfg.rdr_num > 6) m_ultra_right_cfg.rdr_num = 6;
	}
	else {
		m_ultra_right_cfg.m_rdr1_btn_sw = false;
		// 隐藏属性
		m_ultra_right_cfg.m_rdr1_btn_handle.SetWindowText("1号无");
		m_ultra_right_cfg.m_rdr1_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_right_cfg.rdr_num--;
		//if (m_ultra_right_cfg.rdr_num < 0) m_ultra_right_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_right_cfg.rdr_num);
	//m_ultra_right_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraRight2()
{
	if (!m_ultra_right_cfg.m_rdr2_btn_sw) {
		m_ultra_right_cfg.m_rdr2_btn_sw = true;
		// 显示属性
		m_ultra_right_cfg.m_rdr2_btn_handle.SetWindowText("2号有");
		// 按钮颜色改成绿色
		m_ultra_right_cfg.m_rdr2_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_right_cfg.rdr_num++;
		//if (m_ultra_right_cfg.rdr_num > 6) m_ultra_right_cfg.rdr_num = 6;
	}
	else {
		m_ultra_right_cfg.m_rdr2_btn_sw = false;
		// 隐藏属性
		m_ultra_right_cfg.m_rdr2_btn_handle.SetWindowText("2号无");
		m_ultra_right_cfg.m_rdr2_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_right_cfg.rdr_num--;
		//if (m_ultra_right_cfg.rdr_num < 0) m_ultra_right_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_right_cfg.rdr_num);
	//m_ultra_right_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraRight3()
{
	if (!m_ultra_right_cfg.m_rdr3_btn_sw) {
		m_ultra_right_cfg.m_rdr3_btn_sw = true;
		// 显示属性
		m_ultra_right_cfg.m_rdr3_btn_handle.SetWindowText("3号有");
		// 按钮颜色改成绿色
		m_ultra_right_cfg.m_rdr3_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_right_cfg.rdr_num++;
		//if (m_ultra_right_cfg.rdr_num > 6) m_ultra_right_cfg.rdr_num = 6;
	}
	else {
		m_ultra_right_cfg.m_rdr3_btn_sw = false;
		// 隐藏属性
		m_ultra_right_cfg.m_rdr3_btn_handle.SetWindowText("3号无");
		m_ultra_right_cfg.m_rdr3_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_right_cfg.rdr_num--;
		//if (m_ultra_right_cfg.rdr_num < 0) m_ultra_right_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_right_cfg.rdr_num);
	//m_ultra_right_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraRight4()
{
	if (!m_ultra_right_cfg.m_rdr4_btn_sw) {
		m_ultra_right_cfg.m_rdr4_btn_sw = true;
		// 显示属性
		m_ultra_right_cfg.m_rdr4_btn_handle.SetWindowText("4号有");
		// 按钮颜色改成绿色
		m_ultra_right_cfg.m_rdr4_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_right_cfg.rdr_num++;
		//if (m_ultra_right_cfg.rdr_num > 6) m_ultra_right_cfg.rdr_num = 6;
	}
	else {
		m_ultra_right_cfg.m_rdr4_btn_sw = false;
		// 隐藏属性
		m_ultra_right_cfg.m_rdr4_btn_handle.SetWindowText("4号无");
		m_ultra_right_cfg.m_rdr4_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_right_cfg.rdr_num--;
		//if (m_ultra_right_cfg.rdr_num < 0) m_ultra_right_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_right_cfg.rdr_num);
	//m_ultra_right_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraRight5()
{
	if (!m_ultra_right_cfg.m_rdr5_btn_sw) {
		m_ultra_right_cfg.m_rdr5_btn_sw = true;
		// 显示属性
		m_ultra_right_cfg.m_rdr5_btn_handle.SetWindowText("5号有");
		// 按钮颜色改成绿色
		m_ultra_right_cfg.m_rdr5_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_right_cfg.rdr_num++;
		//if (m_ultra_right_cfg.rdr_num > 6) m_ultra_right_cfg.rdr_num = 6;
	}
	else {
		m_ultra_right_cfg.m_rdr5_btn_sw = false;
		// 隐藏属性
		m_ultra_right_cfg.m_rdr5_btn_handle.SetWindowText("5号无");
		m_ultra_right_cfg.m_rdr5_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_right_cfg.rdr_num--;
		//if (m_ultra_right_cfg.rdr_num < 0) m_ultra_right_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_right_cfg.rdr_num);
	//m_ultra_right_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnUltraRight6()
{
	if (!m_ultra_right_cfg.m_rdr6_btn_sw) {
		m_ultra_right_cfg.m_rdr6_btn_sw = true;
		// 显示属性
		m_ultra_right_cfg.m_rdr6_btn_handle.SetWindowText("6号有");
		// 按钮颜色改成绿色
		m_ultra_right_cfg.m_rdr6_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//m_ultra_right_cfg.rdr_num++;
		//if (m_ultra_right_cfg.rdr_num > 6) m_ultra_right_cfg.rdr_num = 6;
	}
	else {
		m_ultra_right_cfg.m_rdr6_btn_sw = false;
		// 隐藏属性
		m_ultra_right_cfg.m_rdr6_btn_handle.SetWindowText("6号无");
		m_ultra_right_cfg.m_rdr6_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//m_ultra_right_cfg.rdr_num--;
		//if (m_ultra_right_cfg.rdr_num < 0) m_ultra_right_cfg.rdr_num = 0;
	}
	//CString str;
	//str.Format("%d", m_ultra_right_cfg.rdr_num);
	//m_ultra_right_num.SetWindowText(str);
}


void CSensorParamHexDlg::OnBnClickedBtnMmwFront1()
{
	if (!m_mmw_front_cfg.m_rdr1_btn_sw) {
		m_mmw_front_cfg.m_rdr1_btn_sw = true;
		// 显示属性
		m_mmw_front_cfg.m_rdr1_btn_handle.SetWindowText("1号有");
		// 按钮颜色改成绿色
		m_mmw_front_cfg.m_rdr1_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//// 雷达数量
		//m_mmw_front_cfg.rdr_num++;
		//if (m_mmw_front_cfg.rdr_num > 1) m_mmw_front_cfg.rdr_num = 1;
	}
	else {
		m_mmw_front_cfg.m_rdr1_btn_sw = false;
		// 隐藏属性
		m_mmw_front_cfg.m_rdr1_btn_handle.SetWindowText("1号无");
		m_mmw_front_cfg.m_rdr1_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//// 雷达数量
		//m_mmw_front_cfg.rdr_num--;
		//if (m_mmw_front_cfg.rdr_num < 0) m_mmw_front_cfg.rdr_num = 0;
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwEnd1()
{
	if (!m_mmw_end_cfg.m_rdr1_btn_sw) {
		m_mmw_end_cfg.m_rdr1_btn_sw = true;
		// 显示属性
		m_mmw_end_cfg.m_rdr1_btn_handle.SetWindowText("1号有");
		// 按钮颜色改成绿色
		m_mmw_end_cfg.m_rdr1_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//// 雷达数量
		//m_mmw_end_cfg.rdr_num++;
		//if (m_mmw_end_cfg.rdr_num > 1) m_mmw_end_cfg.rdr_num = 1;
	}
	else {
		m_mmw_end_cfg.m_rdr1_btn_sw = false;
		// 隐藏属性
		m_mmw_end_cfg.m_rdr1_btn_handle.SetWindowText("1号无");
		m_mmw_end_cfg.m_rdr1_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//// 雷达数量
		//m_mmw_end_cfg.rdr_num--;
		//if (m_mmw_end_cfg.rdr_num < 0) m_mmw_end_cfg.rdr_num = 0;
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwLeft1()
{
	if (!m_mmw_left_cfg.m_rdr1_btn_sw) {
		m_mmw_left_cfg.m_rdr1_btn_sw = true;
		// 显示属性
		m_mmw_left_cfg.m_rdr1_btn_handle.SetWindowText("1号有");
		// 按钮颜色改成绿色
		m_mmw_left_cfg.m_rdr1_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//// 雷达数量
		//m_mmw_left_cfg.rdr_num++;
		//if (m_mmw_left_cfg.rdr_num > 1) m_mmw_left_cfg.rdr_num = 1;
	}
	else {
		m_mmw_left_cfg.m_rdr1_btn_sw = false;
		// 隐藏属性
		m_mmw_left_cfg.m_rdr1_btn_handle.SetWindowText("1号无");
		m_mmw_left_cfg.m_rdr1_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//// 雷达数量
		//m_mmw_left_cfg.rdr_num--;
		//if (m_mmw_left_cfg.rdr_num < 0) m_mmw_left_cfg.rdr_num = 0;
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwLeft2()
{
	if (!m_mmw_left_cfg.m_rdr2_btn_sw) {
		m_mmw_left_cfg.m_rdr2_btn_sw = true;
		// 显示属性
		m_mmw_left_cfg.m_rdr2_btn_handle.SetWindowText("2号有");
		// 按钮颜色改成绿色
		m_mmw_left_cfg.m_rdr2_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//// 雷达数量
		//m_mmw_left_cfg.rdr_num++;
		//if (m_mmw_left_cfg.rdr_num > 1) m_mmw_left_cfg.rdr_num = 1;
	}
	else {
		m_mmw_left_cfg.m_rdr2_btn_sw = false;
		// 隐藏属性
		m_mmw_left_cfg.m_rdr2_btn_handle.SetWindowText("2号无");
		m_mmw_left_cfg.m_rdr2_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//// 雷达数量
		//m_mmw_left_cfg.rdr_num--;
		//if (m_mmw_left_cfg.rdr_num < 0) m_mmw_left_cfg.rdr_num = 0;
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwRight1()
{
	if (!m_mmw_right_cfg.m_rdr1_btn_sw) {
		m_mmw_right_cfg.m_rdr1_btn_sw = true;
		// 显示属性
		m_mmw_right_cfg.m_rdr1_btn_handle.SetWindowText("1号有");
		// 按钮颜色改成绿色
		m_mmw_right_cfg.m_rdr1_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//// 雷达数量
		//m_mmw_right_cfg.rdr_num++;
		//if (m_mmw_right_cfg.rdr_num > 1) m_mmw_right_cfg.rdr_num = 1;
	}
	else {
		m_mmw_right_cfg.m_rdr1_btn_sw = false;
		// 隐藏属性
		m_mmw_right_cfg.m_rdr1_btn_handle.SetWindowText("1号无");
		m_mmw_right_cfg.m_rdr1_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//// 雷达数量
		//m_mmw_right_cfg.rdr_num--;
		//if (m_mmw_right_cfg.rdr_num < 0) m_mmw_right_cfg.rdr_num = 0;
	}
}


void CSensorParamHexDlg::OnBnClickedBtnMmwRight2()
{
	if (!m_mmw_right_cfg.m_rdr2_btn_sw) {
		m_mmw_right_cfg.m_rdr2_btn_sw = true;
		// 显示属性
		m_mmw_right_cfg.m_rdr2_btn_handle.SetWindowText("2号有");
		// 按钮颜色改成绿色
		m_mmw_right_cfg.m_rdr2_btn_handle.SetFaceColor(ENABLE_COLOUR);
		//// 雷达数量
		//m_mmw_right_cfg.rdr_num++;
		//if (m_mmw_right_cfg.rdr_num > 1) m_mmw_right_cfg.rdr_num = 1;
	}
	else {
		m_mmw_right_cfg.m_rdr2_btn_sw = false;
		// 隐藏属性
		m_mmw_right_cfg.m_rdr2_btn_handle.SetWindowText("2号无");
		m_mmw_right_cfg.m_rdr2_btn_handle.SetFaceColor(DEFAULT_COLOUR);
		//// 雷达数量
		//m_mmw_right_cfg.rdr_num--;
		//if (m_mmw_right_cfg.rdr_num < 0) m_mmw_right_cfg.rdr_num = 0;
	}
}


void CSensorParamHexDlg::OnBnClickedMbtnUltraDis()
{
	//printf("%s, %d\n", __FUNCTION__, __LINE__);
	if (!m_ultra_dis_btn_sw)
	{
		m_ultra_dis_btn_sw = true;
	
		m_btn_ultra_dis_handle.SetFaceColor(ENABLE_COLOUR);
		m_ultra_dis_dlg->ShowWindow(SW_SHOW);
		m_sta_result_show_handle.SetWindowText("超声波雷达安装示意图已打开。");
	}
	else
	{
		m_ultra_dis_btn_sw = false;
		m_btn_ultra_dis_handle.SetFaceColor(DEFAULT_COLOUR);
		m_ultra_dis_dlg->ShowWindow(SW_HIDE);
		m_sta_result_show_handle.SetWindowText("超声波雷达安装示意图已关闭。");
	}
}


void CSensorParamHexDlg::OnBnClickedMbtnMmwDis()
{
	//printf("%s, %d\n", __FUNCTION__, __LINE__);
	if (!m_mmw_dis_btn_sw)
	{
		m_mmw_dis_btn_sw = true;

		m_btn_mmw_dis_handle.SetFaceColor(ENABLE_COLOUR);
		m_mmw_dis_dlg->ShowWindow(SW_SHOW);
		m_sta_result_show_handle.SetWindowText("毫米波雷达安装示意图已打开。");
	}
	else
	{
		m_mmw_dis_btn_sw = false;
		m_btn_mmw_dis_handle.SetFaceColor(DEFAULT_COLOUR);
		m_mmw_dis_dlg->ShowWindow(SW_HIDE);
		m_sta_result_show_handle.SetWindowText("毫米波雷达安装示意图已关闭。");
	}
}

bool CSensorParamHexDlg::Write_Ini_Config_File()
{
	char l_type_str[20] = { 0 };

	// 项目类型
	m_iniFile_handle.SetString("PROJECT_TYPE", "use_project_type", m_cfg_p.m_project_type.m_type[m_cfg_p.m_project_type.m_type_index]);

	// 产品类型
	m_iniFile_handle.SetString("PRODUCT_TYPE", "use_product_type", m_cfg_p.m_product_type.m_type[m_cfg_p.m_product_type.m_type_index]);

	// 4G
	m_iniFile_handle.SetNumber("4G", "func_sw", m_cfg_p.m_4g.m_func_sw);
	m_iniFile_handle.SetString("4G", "use_producer", m_cfg_p.m_4g.m_producer[m_cfg_p.m_4g.m_producer_index]);

	// GPS
	m_iniFile_handle.SetNumber("GPS", "func_sw", m_cfg_p.m_gps.m_func_sw);
	m_iniFile_handle.SetString("GPS", "use_producer", m_cfg_p.m_gps.m_producer[m_cfg_p.m_gps.m_producer_index]);

	// IMU
	m_iniFile_handle.SetNumber("IMU", "func_sw", m_cfg_p.m_imu.m_func_sw);
	m_iniFile_handle.SetString("IMU", "use_producer", m_cfg_p.m_imu.m_producer[m_cfg_p.m_imu.m_producer_index]);

	// DBL_CAM
	m_iniFile_handle.SetNumber("DBL_CAM", "func_sw", m_cfg_p.m_dbl_cam.m_func_sw);
	m_iniFile_handle.SetString("DBL_CAM", "use_producer", m_cfg_p.m_dbl_cam.m_producer[m_cfg_p.m_dbl_cam.m_producer_index]);

	// SGL_CAM
	m_iniFile_handle.SetNumber("SGL_CAM", "func_sw", m_cfg_p.m_sgl_cam.m_func_sw);
	m_iniFile_handle.SetString("SGL_CAM", "use_producer", m_cfg_p.m_sgl_cam.m_producer[m_cfg_p.m_sgl_cam.m_producer_index]);

	// CAR_DVR
	m_iniFile_handle.SetNumber("CAR_DVR", "func_sw", m_cfg_p.m_car_dvr.m_func_sw);
	m_iniFile_handle.SetString("CAR_DVR", "use_producer", m_cfg_p.m_car_dvr.m_producer[m_cfg_p.m_car_dvr.m_producer_index]);

	// BRK_ACT
	m_iniFile_handle.SetNumber("BRK_ACT", "func_sw", m_cfg_p.m_brk_act.m_func_sw);
	m_iniFile_handle.SetString("BRK_ACT", "use_producer", m_cfg_p.m_brk_act.m_producer[m_cfg_p.m_brk_act.m_producer_index]);

	// ULTRA_FRONT
	m_iniFile_handle.SetNumber("ULTRA_FRONT", "func_sw", m_cfg_p.m_ultra_front.m_func_sw);
	m_iniFile_handle.SetString("ULTRA_FRONT", "use_producer", m_cfg_p.m_ultra_front.m_producer[m_cfg_p.m_ultra_front.m_producer_index]);
	m_iniFile_handle.SetNumber("ULTRA_FRONT", "controlerID", m_cfg_p.m_ultra_front.controler_id);
	// 单个雷达状态
	m_iniFile_handle.SetNumber("ULTRA_FRONT", "rdr1", m_cfg_p.m_ultra_front.m_rdr1_sw);
	m_iniFile_handle.SetNumber("ULTRA_FRONT", "rdr2", m_cfg_p.m_ultra_front.m_rdr2_sw);
	m_iniFile_handle.SetNumber("ULTRA_FRONT", "rdr3", m_cfg_p.m_ultra_front.m_rdr3_sw);
	m_iniFile_handle.SetNumber("ULTRA_FRONT", "rdr4", m_cfg_p.m_ultra_front.m_rdr4_sw);
	m_iniFile_handle.SetNumber("ULTRA_FRONT", "rdr5", m_cfg_p.m_ultra_front.m_rdr5_sw);
	m_iniFile_handle.SetNumber("ULTRA_FRONT", "rdr6", m_cfg_p.m_ultra_front.m_rdr6_sw);


	// ULTRA_END
	m_iniFile_handle.SetNumber("ULTRA_END", "func_sw", m_cfg_p.m_ultra_end.m_func_sw);
	m_iniFile_handle.SetString("ULTRA_END", "use_producer", m_cfg_p.m_ultra_end.m_producer[m_cfg_p.m_ultra_end.m_producer_index]);
	m_iniFile_handle.SetNumber("ULTRA_END", "controlerID", m_cfg_p.m_ultra_end.controler_id);
	// 单个雷达状态
	m_iniFile_handle.SetNumber("ULTRA_END", "rdr1", m_cfg_p.m_ultra_end.m_rdr1_sw);
	m_iniFile_handle.SetNumber("ULTRA_END", "rdr2", m_cfg_p.m_ultra_end.m_rdr2_sw);
	m_iniFile_handle.SetNumber("ULTRA_END", "rdr3", m_cfg_p.m_ultra_end.m_rdr3_sw);
	m_iniFile_handle.SetNumber("ULTRA_END", "rdr4", m_cfg_p.m_ultra_end.m_rdr4_sw);
	m_iniFile_handle.SetNumber("ULTRA_END", "rdr5", m_cfg_p.m_ultra_end.m_rdr5_sw);
	m_iniFile_handle.SetNumber("ULTRA_END", "rdr6", m_cfg_p.m_ultra_end.m_rdr6_sw);

	// ULTRA_LEFT
	m_iniFile_handle.SetNumber("ULTRA_LEFT", "func_sw", m_cfg_p.m_ultra_left.m_func_sw);
	m_iniFile_handle.SetString("ULTRA_LEFT", "use_producer", m_cfg_p.m_ultra_left.m_producer[m_cfg_p.m_ultra_left.m_producer_index]);
	m_iniFile_handle.SetNumber("ULTRA_LEFT", "controlerID", m_cfg_p.m_ultra_left.controler_id);
	// 单个雷达状态
	m_iniFile_handle.SetNumber("ULTRA_LEFT", "rdr1", m_cfg_p.m_ultra_left.m_rdr1_sw);
	m_iniFile_handle.SetNumber("ULTRA_LEFT", "rdr2", m_cfg_p.m_ultra_left.m_rdr2_sw);
	m_iniFile_handle.SetNumber("ULTRA_LEFT", "rdr3", m_cfg_p.m_ultra_left.m_rdr3_sw);
	m_iniFile_handle.SetNumber("ULTRA_LEFT", "rdr4", m_cfg_p.m_ultra_left.m_rdr4_sw);
	m_iniFile_handle.SetNumber("ULTRA_LEFT", "rdr5", m_cfg_p.m_ultra_left.m_rdr5_sw);
	m_iniFile_handle.SetNumber("ULTRA_LEFT", "rdr6", m_cfg_p.m_ultra_left.m_rdr6_sw);

	// ULTRA_RIGHT
	m_iniFile_handle.SetNumber("ULTRA_RIGHT", "func_sw", m_cfg_p.m_ultra_right.m_func_sw);
	m_iniFile_handle.SetString("ULTRA_RIGHT", "use_producer", m_cfg_p.m_ultra_right.m_producer[m_cfg_p.m_ultra_right.m_producer_index]);
	m_iniFile_handle.SetNumber("ULTRA_RIGHT", "controlerID", m_cfg_p.m_ultra_right.controler_id);
	// 单个雷达状态
	m_iniFile_handle.SetNumber("ULTRA_RIGHT", "rdr1", m_cfg_p.m_ultra_right.m_rdr1_sw);
	m_iniFile_handle.SetNumber("ULTRA_RIGHT", "rdr2", m_cfg_p.m_ultra_right.m_rdr2_sw);
	m_iniFile_handle.SetNumber("ULTRA_RIGHT", "rdr3", m_cfg_p.m_ultra_right.m_rdr3_sw);
	m_iniFile_handle.SetNumber("ULTRA_RIGHT", "rdr4", m_cfg_p.m_ultra_right.m_rdr4_sw);
	m_iniFile_handle.SetNumber("ULTRA_RIGHT", "rdr5", m_cfg_p.m_ultra_right.m_rdr5_sw);
	m_iniFile_handle.SetNumber("ULTRA_RIGHT", "rdr6", m_cfg_p.m_ultra_right.m_rdr6_sw);

	// MMW_FRONT
	m_iniFile_handle.SetNumber("MMW_FRONT", "func_sw", m_cfg_p.m_mmw_front.m_func_sw);
	m_iniFile_handle.SetString("MMW_FRONT", "use_producer", m_cfg_p.m_mmw_front.m_producer[m_cfg_p.m_mmw_front.m_producer_index]);
	// 单个雷达状态
	m_iniFile_handle.SetNumber("MMW_FRONT", "rdr1", m_cfg_p.m_mmw_front.m_rdr1_sw);

	// MMW_END
	m_iniFile_handle.SetNumber("MMW_END", "func_sw", m_cfg_p.m_mmw_end.m_func_sw);
	m_iniFile_handle.SetString("MMW_END", "use_producer", m_cfg_p.m_mmw_end.m_producer[m_cfg_p.m_mmw_end.m_producer_index]);
	// 单个雷达状态
	m_iniFile_handle.SetNumber("MMW_END", "rdr1", m_cfg_p.m_mmw_end.m_rdr1_sw);

	// MMW_LEFT
	m_iniFile_handle.SetNumber("MMW_LEFT", "func_sw", m_cfg_p.m_mmw_left.m_func_sw);
	m_iniFile_handle.SetString("MMW_LEFT", "use_producer", m_cfg_p.m_mmw_left.m_producer[m_cfg_p.m_mmw_left.m_producer_index]);
	// 单个雷达状态
	m_iniFile_handle.SetNumber("MMW_LEFT", "rdr1", m_cfg_p.m_mmw_left.m_rdr1_sw);
	m_iniFile_handle.SetNumber("MMW_LEFT", "rdr2", m_cfg_p.m_mmw_left.m_rdr2_sw);

	// MMW_RIGHT
	m_iniFile_handle.SetNumber("MMW_RIGHT", "func_sw", m_cfg_p.m_mmw_right.m_func_sw);
	m_iniFile_handle.SetString("MMW_RIGHT", "use_producer", m_cfg_p.m_mmw_right.m_producer[m_cfg_p.m_mmw_right.m_producer_index]);
	// 单个雷达状态
	m_iniFile_handle.SetNumber("MMW_RIGHT", "rdr1", m_cfg_p.m_mmw_right.m_rdr1_sw);
	m_iniFile_handle.SetNumber("MMW_RIGHT", "rdr2", m_cfg_p.m_mmw_right.m_rdr2_sw);

	return false;
}

void CSensorParamHexDlg::Ini_File_Config_Param_To_Hex_Config_Param(uint8_t *data)
{
#if 1 // 按照ASCII协议中的数据内容部分进行存储
	//uint8_t data[512] = { 0 };
	//memset(data, 0, 512);

	// 4G 模块  4~6bit预留和第二个字节保留 
	data[0] = (m_cfg_p.m_4g.m_producer_index) & 0x0F;			// 0~3bit 厂家
	data[0] |= (uint8_t)m_cfg_p.m_4g.m_func_sw << 7;				// 7bit 功能开关
	data[1] = 0x00;

	// GPS 模块  4~6bit预留和第二个字节保留 
	data[2] = (m_cfg_p.m_gps.m_producer_index) & 0x0F;			// 0~3bit 厂家
	data[2] |= (uint8_t)m_cfg_p.m_gps.m_func_sw << 7;				// 7bit 功能开关
	data[3] = 0x00;

	// 双目相机 模块 4~6bit预留和第二个字节保留 
	data[4] = (m_cfg_p.m_dbl_cam.m_producer_index) & 0x0F;			// 0~3bit 厂家
	data[4] |= (uint8_t)m_cfg_p.m_dbl_cam.m_func_sw << 7;				// 7bit 功能开关
	data[5] = 0x00;

	// 单目相机 模块 4~6bit预留和第二个字节保留 
	data[6] = (m_cfg_p.m_sgl_cam.m_producer_index) & 0x0F;			// 0~3bit 厂家
	data[6] |= (uint8_t)m_cfg_p.m_sgl_cam.m_func_sw << 7;				// 7bit 功能开关
	data[7] = 0x00;

	// 前方超声波雷达 模块 第二个字节7bit保留
	data[8] = (uint8_t)m_cfg_p.m_ultra_front.m_func_sw;						// 1bit 功能开关
	data[8] |= ((m_cfg_p.m_ultra_front.m_producer_index) & 0x07) << 1;	// 1~3bit 厂家
	data[8] |= (m_cfg_p.m_ultra_front.m_sensor_num & 0x07) << 4;			// 4~6bit 传感器数量
	data[8] |= m_cfg_p.m_ultra_front.controler_id << 7;						// 7bit 控制器的ID 低位
	data[9] = m_cfg_p.m_ultra_front.controler_id >> 1;						// 0bit 控制器ID 高位
	data[9] |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr1_sw) << 1;				// 1bit 1号雷达开关
	data[9] |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr2_sw) << 2;				// 2bit 2号雷达开关
	data[9] |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr3_sw) << 3;				// 3bit 3号雷达开关
	data[9] |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr4_sw) << 4;				// 4bit 4号雷达开关
	data[9] |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr5_sw) << 5;				// 5bit 5号雷达开关
	data[9] |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr6_sw) << 6;				// 6bit 6号雷达开关
	data[9] &= 0x7F;														// 7bit 保留

	// 后方超声波雷达 模块 第二个字节7bit保留
	data[10] = (uint8_t)m_cfg_p.m_ultra_end.m_func_sw;						// 1bit 功能开关
	data[10] |= ((m_cfg_p.m_ultra_end.m_producer_index) & 0x07) << 1;	// 1~3bit 厂家
	data[10] |= (m_cfg_p.m_ultra_end.m_sensor_num & 0x07) << 4;				// 4~6bit 传感器数量
	data[10] |= m_cfg_p.m_ultra_end.controler_id << 7;						// 7bit 控制器的ID 低位
	data[11] = m_cfg_p.m_ultra_end.controler_id >> 1;						// 0bit 控制器ID 高位
	data[11] |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr1_sw) << 1;				// 1bit 1号雷达开关
	data[11] |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr2_sw) << 2;				// 2bit 2号雷达开关
	data[11] |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr3_sw) << 3;				// 3bit 3号雷达开关
	data[11] |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr4_sw) << 4;				// 4bit 4号雷达开关
	data[11] |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr5_sw) << 5;				// 5bit 5号雷达开关
	data[11] |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr6_sw) << 6;				// 6bit 6号雷达开关
	data[11] &= 0x7F;														// 7bit 保留

	// 左侧超声波雷达 模块 第二个字节7bit保留
	data[12] = (uint8_t)m_cfg_p.m_ultra_left.m_func_sw;						// 1bit 功能开关
	data[12] |= ((m_cfg_p.m_ultra_left.m_producer_index) & 0x07) << 1;	// 1~3bit 厂家
	data[12] |= (m_cfg_p.m_ultra_left.m_sensor_num & 0x07) << 4;			// 4~6bit 传感器数量
	data[12] |= m_cfg_p.m_ultra_left.controler_id << 7;						// 7bit 控制器的ID 低位
	data[13] = m_cfg_p.m_ultra_left.controler_id >> 1;						// 0bit 控制器ID 高位
	data[13] |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr1_sw) << 1;				// 1bit 1号雷达开关
	data[13] |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr2_sw) << 2;				// 2bit 2号雷达开关
	data[13] |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr3_sw) << 3;				// 3bit 3号雷达开关
	data[13] |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr4_sw) << 4;				// 4bit 4号雷达开关
	data[13] |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr5_sw) << 5;				// 5bit 5号雷达开关
	data[13] |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr6_sw) << 6;				// 6bit 6号雷达开关
	data[13] &= 0x7F;														// 7bit 保留
	//printf("Ultra Left low:%02X\n", data[12]);
	//printf("Ultra Left high:%02X\n", data[13]);
	// 右侧超声波雷达 模块 第二个字节7bit保留
	data[14] = (uint8_t)m_cfg_p.m_ultra_right.m_func_sw;					// 1bit 功能开关
	data[14] |= ((m_cfg_p.m_ultra_right.m_producer_index ) & 0x07) << 1;	// 1~3bit 厂家
	data[14] |= (m_cfg_p.m_ultra_right.m_sensor_num & 0x07) << 4;			// 4~6bit 传感器数量
	data[14] |= m_cfg_p.m_ultra_right.controler_id << 7;					// 7bit 控制器的ID 低位
	data[15] = m_cfg_p.m_ultra_right.controler_id >> 1;						// 0bit 控制器ID 高位
	data[15] |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr1_sw) << 1;			// 1bit 1号雷达开关
	data[15] |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr2_sw) << 2;			// 2bit 2号雷达开关
	data[15] |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr3_sw) << 3;			// 3bit 3号雷达开关
	data[15] |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr4_sw) << 4;			// 4bit 4号雷达开关
	data[15] |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr5_sw) << 5;			// 5bit 5号雷达开关
	data[15] |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr6_sw) << 6;			// 6bit 6号雷达开关
	data[15] &= 0x7F;														// 7bit 保留

	// 前方毫米波雷达 第一个字节6~7bit保留 第二个字节1~7bit保留
	data[16] = (uint8_t)m_cfg_p.m_mmw_front.m_func_sw;						// 1bit 功能开关
	data[16] |= ((m_cfg_p.m_mmw_front.m_producer_index) & 0x07) << 1;	// 1~3bit 厂家
	data[16] |= (m_cfg_p.m_mmw_front.m_sensor_num & 0x07) << 4;				// 4~5bit 传感器数量
	data[16] &= 0x3F;														// 6~7bit 预留
	data[17] = (uint8_t)m_cfg_p.m_mmw_front.m_rdr1_sw;						// 0bit 1号雷达开关
	data[17] &= 0x01;														// 1~7bit 预留

	// 后方毫米波雷达 第一个字节6~7bit保留 第二个字节1~7bit保留
	data[18] = (uint8_t)m_cfg_p.m_mmw_end.m_func_sw;						// 1bit 功能开关
	data[18] |= ((m_cfg_p.m_mmw_end.m_producer_index) & 0x07) << 1;		// 1~3bit 厂家
	data[18] |= (m_cfg_p.m_mmw_end.m_sensor_num & 0x07) << 4;				// 4~5bit 传感器数量
	data[18] &= 0x3F;														// 6~7bit 预留
	data[19] = (uint8_t)m_cfg_p.m_mmw_end.m_rdr1_sw;						// 0bit 1号雷达开关
	data[19] &= 0x01;														// 1~7bit 预留

	// 左侧毫米波雷达 第一个字节6~7bit保留 第二个字节1~7bit保留
	data[20] = (uint8_t)m_cfg_p.m_mmw_left.m_func_sw;						// 1bit 功能开关
	data[20] |= ((m_cfg_p.m_mmw_left.m_producer_index) & 0x07) << 1;	// 1~3bit 厂家
	data[20] |= (m_cfg_p.m_mmw_left.m_sensor_num & 0x07) << 4;				// 4~5bit 传感器数量
	data[20] &= 0x3F;														// 6~7bit 预留
	data[21] = (uint8_t)m_cfg_p.m_mmw_left.m_rdr1_sw;						// 0bit 1号雷达开关
	data[21] &= 0x01;														// 1~7bit 预留

	// 右侧毫米波雷达 第一个字节6~7bit保留 第二个字节1~7bit保留
	data[22] = (uint8_t)m_cfg_p.m_mmw_right.m_func_sw;						// 1bit 功能开关
	data[22] |= ((m_cfg_p.m_mmw_right.m_producer_index) & 0x07) << 1;	// 1~3bit 厂家
	data[22] |= (m_cfg_p.m_mmw_right.m_sensor_num & 0x07) << 4;				// 4~5bit 传感器数量
	data[22] &= 0x3F;														// 6~7bit 预留
	data[23] = (uint8_t)m_cfg_p.m_mmw_right.m_rdr1_sw;						// 0bit 1号雷达开关
	data[23] &= 0x01;														// 1~7bit 预留

	// IMU 模块  4~6bit预留和第二个字节保留 
	data[24] = (m_cfg_p.m_imu.m_producer_index) & 0x0F;					// 0~3bit 厂家
	data[24] |= (uint8_t)m_cfg_p.m_imu.m_func_sw << 7;						// 7bit 功能开关
	data[25] = 0x00;

	// 行车记录仪 模块  4~6bit预留和第二个字节保留 
	data[26] = (m_cfg_p.m_car_dvr.m_producer_index) & 0x0F;				// 0~3bit 厂家
	data[26] |= (uint8_t)m_cfg_p.m_car_dvr.m_func_sw << 7;					// 7bit 功能开关
	data[27] = 0x00;

	// 制动机构 模块  4~6bit预留和第二个字节保留 
	data[28] = (m_cfg_p.m_brk_act.m_producer_index) & 0x0F;				// 0~3bit 厂家
	data[28] |= (uint8_t)m_cfg_p.m_brk_act.m_func_sw << 7;					// 7bit 功能开关
	data[29] = 0x00;

	// 项目类型
	data[30] = m_cfg_p.m_project_type.m_type_index;

	// 产品类型
	data[31] = m_cfg_p.m_product_type.m_type_index;

	uint8_t clac_chk = 0;
	for (int i = 0; i < 512; i++) {
		clac_chk += data[i] & 0xFF;
	}
	data[510] = clac_chk;

#else
	// 项目类型
	m_hex_p.m_project = m_cfg_p.m_project_type.m_type_index;

	// 产品类型
	m_hex_p.m_product = m_cfg_p.m_product_type.m_type_index;

	// 4G模块
	m_hex_p.m_4g.m_isOpen = m_cfg_p.m_4g.m_func_sw;								// 功能开关
	m_hex_p.m_4g.m_producer = m_cfg_p.m_4g.m_producer_index;				// 厂家

	// GPS模块
	m_hex_p.m_gps.m_isOpen = m_cfg_p.m_gps.m_func_sw;							// 功能开关
	m_hex_p.m_gps.m_producer = m_cfg_p.m_gps.m_producer_index;				// 厂家

	// IMU模块
	m_hex_p.m_imu.m_isOpen = m_cfg_p.m_imu.m_func_sw;							// 功能开关
	m_hex_p.m_imu.m_producer = m_cfg_p.m_imu.m_producer_index;				// 厂家

	// 双目模块
	m_hex_p.m_dbl_cam.m_isOpen = m_cfg_p.m_dbl_cam.m_func_sw;					// 功能开关
	m_hex_p.m_dbl_cam.m_producer = m_cfg_p.m_dbl_cam.m_producer_index;		// 厂家

	// 单目模块
	m_hex_p.m_sgl_cam.m_isOpen = m_cfg_p.m_sgl_cam.m_func_sw;					// 功能开关
	m_hex_p.m_sgl_cam.m_producer = m_cfg_p.m_sgl_cam.m_producer_index;		// 厂家

	// 行车记录仪模块
	m_hex_p.m_car_dvr.m_isOpen = m_cfg_p.m_car_dvr.m_func_sw;					// 功能开关
	m_hex_p.m_car_dvr.m_producer = m_cfg_p.m_car_dvr.m_producer_index;		// 厂家

	// 刹车制动机构模块
	m_hex_p.m_actuator.m_isOpen = m_cfg_p.m_brk_act.m_func_sw;					// 功能开关
	m_hex_p.m_actuator.m_producer = m_cfg_p.m_brk_act.m_producer_index;		// 厂家

	// 前方超声波雷达
	m_hex_p.m_ultra_radar.m_front.m_main_sw = m_cfg_p.m_ultra_front.m_func_sw;				// 功能开关
	m_hex_p.m_ultra_radar.m_front.m_producer = m_cfg_p.m_ultra_front.m_producer_index;	// 厂家编号
	m_hex_p.m_ultra_radar.m_front.m_controler = m_cfg_p.m_ultra_front.controler_id;		// 控制器编号
	m_hex_p.m_ultra_radar.m_front.m_rdr_sw = 0;												// 单个雷达使能开关
	m_hex_p.m_ultra_radar.m_front.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr6_sw & 0x01) << 5;
	m_hex_p.m_ultra_radar.m_front.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr5_sw & 0x01) << 4;
	m_hex_p.m_ultra_radar.m_front.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr4_sw & 0x01) << 3;
	m_hex_p.m_ultra_radar.m_front.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr3_sw & 0x01) << 2;
	m_hex_p.m_ultra_radar.m_front.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr2_sw & 0x01) << 1;
	m_hex_p.m_ultra_radar.m_front.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr1_sw & 0x01) ;

	// 后方超声波雷达
	m_hex_p.m_ultra_radar.m_end.m_main_sw = m_cfg_p.m_ultra_end.m_func_sw;				// 功能开关
	m_hex_p.m_ultra_radar.m_end.m_producer = m_cfg_p.m_ultra_end.m_producer_index;	// 厂家编号
	m_hex_p.m_ultra_radar.m_end.m_controler = m_cfg_p.m_ultra_end.controler_id;		// 控制器编号
	m_hex_p.m_ultra_radar.m_end.m_rdr_sw = 0;												// 单个雷达使能开关
	m_hex_p.m_ultra_radar.m_end.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr6_sw & 0x01) << 5;
	m_hex_p.m_ultra_radar.m_end.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr5_sw & 0x01) << 4;
	m_hex_p.m_ultra_radar.m_end.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr4_sw & 0x01) << 3;
	m_hex_p.m_ultra_radar.m_end.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr3_sw & 0x01) << 2;
	m_hex_p.m_ultra_radar.m_end.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr1_sw & 0x01);
	m_hex_p.m_ultra_radar.m_end.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_end.m_rdr2_sw & 0x01) << 1;

	// 左侧超声波雷达
	m_hex_p.m_ultra_radar.m_left.m_main_sw = m_cfg_p.m_ultra_left.m_func_sw;				// 功能开关
	m_hex_p.m_ultra_radar.m_left.m_producer = m_cfg_p.m_ultra_left.m_producer_index;	// 厂家编号
	m_hex_p.m_ultra_radar.m_left.m_controler = m_cfg_p.m_ultra_left.controler_id;		// 控制器编号
	m_hex_p.m_ultra_radar.m_left.m_rdr_sw = 0;												// 单个雷达使能开关
	m_hex_p.m_ultra_radar.m_left.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr6_sw & 0x01) << 5;
	m_hex_p.m_ultra_radar.m_left.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr5_sw & 0x01) << 4;
	m_hex_p.m_ultra_radar.m_left.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr4_sw & 0x01) << 3;
	m_hex_p.m_ultra_radar.m_left.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr3_sw & 0x01) << 2;
	m_hex_p.m_ultra_radar.m_left.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr2_sw & 0x01) << 1;
	m_hex_p.m_ultra_radar.m_left.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_left.m_rdr1_sw & 0x01);

	// 右侧超声波雷达
	m_hex_p.m_ultra_radar.m_right.m_main_sw = m_cfg_p.m_ultra_right.m_func_sw;				// 功能开关
	m_hex_p.m_ultra_radar.m_right.m_producer = m_cfg_p.m_ultra_right.m_producer_index;	// 厂家编号
	m_hex_p.m_ultra_radar.m_right.m_controler = m_cfg_p.m_ultra_right.controler_id;		// 控制器编号
	m_hex_p.m_ultra_radar.m_right.m_rdr_sw = 0;												// 单个雷达使能开关
	m_hex_p.m_ultra_radar.m_right.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr6_sw & 0x01) << 5;
	m_hex_p.m_ultra_radar.m_right.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr5_sw & 0x01) << 4;
	m_hex_p.m_ultra_radar.m_right.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr4_sw & 0x01) << 3;
	m_hex_p.m_ultra_radar.m_right.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr3_sw & 0x01) << 2;
	m_hex_p.m_ultra_radar.m_right.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr2_sw & 0x01) << 1;
	m_hex_p.m_ultra_radar.m_right.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_right.m_rdr1_sw & 0x01);

	// 前方毫米波雷达
	m_hex_p.m_mmw_radar.m_front.m_main_sw = m_cfg_p.m_mmw_front.m_func_sw;					// 功能开关
	m_hex_p.m_mmw_radar.m_front.m_producer = m_cfg_p.m_mmw_front.m_producer_index;		// 厂家编号
	m_hex_p.m_mmw_radar.m_front.m_rdr_sw = 0;												// 单个雷达使能开关
	m_hex_p.m_mmw_radar.m_front.m_rdr_sw |= ((uint8_t)m_cfg_p.m_ultra_front.m_rdr1_sw & 0x01);

	// 后方毫米波雷达
	m_hex_p.m_mmw_radar.m_end.m_main_sw = m_cfg_p.m_mmw_end.m_func_sw;					// 功能开关
	m_hex_p.m_mmw_radar.m_end.m_producer = m_cfg_p.m_mmw_end.m_producer_index;		// 厂家编号
	m_hex_p.m_mmw_radar.m_end.m_rdr_sw = 0;												// 单个雷达使能开关
	m_hex_p.m_mmw_radar.m_end.m_rdr_sw |= ((uint8_t)m_cfg_p.m_mmw_end.m_rdr1_sw & 0x01);

	// 左侧毫米波雷达
	m_hex_p.m_mmw_radar.m_left.m_main_sw = m_cfg_p.m_mmw_left.m_func_sw;					// 功能开关
	m_hex_p.m_mmw_radar.m_left.m_producer = m_cfg_p.m_mmw_left.m_producer_index;		// 厂家编号
	m_hex_p.m_mmw_radar.m_left.m_rdr_sw = 0;												// 单个雷达使能开关
	m_hex_p.m_mmw_radar.m_left.m_rdr_sw |= ((uint8_t)m_cfg_p.m_mmw_left.m_rdr1_sw & 0x01);
	m_hex_p.m_mmw_radar.m_left.m_rdr_sw |= ((uint8_t)m_cfg_p.m_mmw_left.m_rdr2_sw & 0x01) << 1;

	// 右侧毫米波雷达
	m_hex_p.m_mmw_radar.m_right.m_main_sw = m_cfg_p.m_mmw_right.m_func_sw;					// 功能开关
	m_hex_p.m_mmw_radar.m_right.m_producer = m_cfg_p.m_mmw_right.m_producer_index;		// 厂家编号
	m_hex_p.m_mmw_radar.m_right.m_rdr_sw = 0;												// 单个雷达使能开关
	m_hex_p.m_mmw_radar.m_right.m_rdr_sw |= ((uint8_t)m_cfg_p.m_mmw_right.m_rdr1_sw & 0x01);
	m_hex_p.m_mmw_radar.m_right.m_rdr_sw |= ((uint8_t)m_cfg_p.m_mmw_right.m_rdr2_sw & 0x01) << 1;

	int param_size = sizeof(m_hex_p);
	memcpy(data, &m_hex_p, sizeof(Sensor_Hex_MGT));
	uint8_t clac_chk = 0;
	for (int i = 0; i < 512; i++) {
		clac_chk += data[i] & 0xFF;
	}
	data[510] = clac_chk;

#endif
	// 测试打印
	//printf("\n------------------------------------------\n");
	//for (int i = 0; i < 512; i++) {
	//	printf("%02X ", data[i] & 0xFF);
	//}
	//
	//printf("\n-------%02X-----------------------------------\n", clac_chk);

}
/*
* 说明：制作Hex格式接口
* 参数1：数据长度
* 参数2：起始地址
* 参数3：数据类型
* 参数4：数据串，最多16个字节
* 参数5：打包好的数据
* 返回：true成功；false失败
*/
bool CSensorParamHexDlg::Make_Hex_Format_API(uint8_t dataLen, uint16_t addr, uint8_t dataType, uint8_t *data, uint8_t *ret_data_str)
{
	uint8_t index = 0;

	// 数据长度 3B 
	sprintf((char*)(ret_data_str + index), ":%02X", dataLen);	
	index += 3;

	// 起始地址	4B
	sprintf((char*)(ret_data_str + index), "%04X", addr);		
	index += 4;

	// 数据类型
	sprintf((char*)(ret_data_str + index), "%02X", dataType);
	index += 2;

	// 数据
	for (uint8_t i = 0; i < dataLen; i++) {
		sprintf((char*)(ret_data_str + index), "%02X", data[i]);
		index += 2;
	}

	// 校验 1B
	uint8_t clac_chk = 0;
	clac_chk += dataLen;
	clac_chk += addr & 0xFF;
	clac_chk += (addr >> 8) & 0xFF;
	clac_chk += dataType;
	for (uint8_t i = 0; i < dataLen; i++) {
		clac_chk += data[i]&0xFF;
	}
	//printf("index:%d, chk:%02X\n", index, 0x100 - clac_chk);

	sprintf((char *)(ret_data_str+index), "%02X", 0x100 - clac_chk);
	index += 2;
	ret_data_str[index] = 0x0D; index += 1;
	//ret_data_str[index] = 0x0A; index += 1;
	ret_data_str[index] = 0;
			
	//printf("%s", ret_data_str);

	// 写到hex文件中

	return true;
}
bool CSensorParamHexDlg::Make_Hex_File(uint8_t *data_w, int data_len)
{
	// 生成一个文件upgradeInfo.txt文件。记录 文件名|文件字节大小|文件版本号
	FILE* pFile_hex;
	char* data_info = (char*)malloc(sizeof(char) * 3 * 1024);

	TCHAR sHexPath[MAX_PATH] = { 0 };
	CString m_file_name("");
	CString m_hex_name("");

	// 获取当前文件路径
	GetCurrentDirectory(MAX_PATH, sHexPath);
	strcpy(m_hexFile_path, sHexPath);

	// 获取Hex文件命名
	CString l_hex_name;
	m_edt_hex_name_handle.GetWindowText(l_hex_name);
	if (l_hex_name.IsEmpty()) {	// 文件为空，采用默认命名 project_Sensor_Cfg.hex
		strcat(m_hexFile_path, "\\Project_Sensor_Cfg.hex");
		m_hex_name.Format("%s", "Project_Sensor_Cfg.hex");
	}
	else {	
		// 判断字符串中是否含有.hex若有就直接使用，否则添加
		if (l_hex_name.FindOneOf(".hex") == -1) {	// 没找到
			strcat(m_hexFile_path, "\\");
			strcat(m_hexFile_path + 1, l_hex_name);
			strcat(m_hexFile_path + 1 + l_hex_name.GetAllocLength(), ".hex");
			m_hex_name.Format("%s.hex", l_hex_name);
		}
		else {
			strcat(m_hexFile_path, "\\");
			strcat(m_hexFile_path + 1, l_hex_name);
			m_hex_name.Format("%s", l_hex_name);
		}
	}

	printf("path:%s\n", m_hexFile_path);

	m_file_name = m_hexFile_path;
	_wfopen_s(&pFile_hex, m_file_name.AllocSysString(), L"w"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
	if (pFile_hex == NULL) //判断文件是否打开成功
	{
		MessageBox("打开文件失败。", "警告", MB_ICONSTOP);
		return false;
	}

	
	// 步骤1：基地址 :020000040007F3
	uint8_t data[16] = { 0x00, 0x07 };
	uint8_t ret_data[50] = { 0 };
	Make_Hex_Format_API(2, 0, 0x04, data, ret_data);
	fwrite(ret_data, sizeof(char), strlen((char*)ret_data), pFile_hex);		// 写操作

	// 步骤2：写数据内容（先写512字节的数据，再写512字节的0xFF）
	// 数据分包 起始地址从0x7F400，写1024个字节
	uint16_t addr_w = 0xF400;
	uint16_t index = 0;
	for (int i = 0; i < data_len / 2 / 16; i++) {
		memset(ret_data, 0, 50);
		Make_Hex_Format_API(16, addr_w, 0x00, data_w + index, ret_data);
		fwrite(ret_data, sizeof(char), strlen((char*)ret_data), pFile_hex);	// 写操作
		addr_w += 16;
		index += 16;
	}

	// 剩余512B 写0xFF
	uint8_t data_0xff[16] = { 0 };
	memset(data_0xff, 0xFF, 16);
	for (int i = data_len / 2 / 16; i < data_len / 16; i++) {
		memset(ret_data, 0, 50);
		Make_Hex_Format_API(16, addr_w, 0x00, data_0xff, ret_data);
		fwrite(ret_data, sizeof(char), strlen((char*)ret_data), pFile_hex);	// 写操作
		addr_w += 16;
	}

	// 步骤3：写剩余部分
	// :020000040C00EE
	uint8_t data_end[4] = {0x0C, 0x00};
	Make_Hex_Format_API(2, 0x0000, 0x04, data_end, ret_data);
	fwrite(ret_data, sizeof(char), strlen((char*)ret_data), pFile_hex);	// 写操作

	// :041000005A5AA5A5EE
	data_end[0] = 0x5A;
	data_end[1] = 0x5A;
	data_end[2] = 0xA5;
	data_end[3] = 0xA5;
	Make_Hex_Format_API(4, 0x1000, 0x00, data_end, ret_data);
	fwrite(ret_data, sizeof(char), strlen((char*)ret_data), pFile_hex);	// 写操作

	// : 041800002143658794
	data_end[0] = 0x21;
	data_end[1] = 0x43;
	data_end[2] = 0x65;
	data_end[3] = 0x87;
	Make_Hex_Format_API(4, 0x1800, 0x00, data_end, ret_data);
	fwrite(ret_data, sizeof(char), strlen((char*)ret_data), pFile_hex);	// 写操作

	// : 00000001FF
	Make_Hex_Format_API(0, 0x0000, 0x01, data_end, ret_data);
	fwrite(ret_data, sizeof(char), strlen((char*)ret_data), pFile_hex);	// 写操作

	fclose(pFile_hex);//关闭文件
	free(data_info);

	m_edt_hex_name_handle.SetWindowText(m_hex_name);
	return true;
}
bool CSensorParamHexDlg::Generating_Hex_File()
{
	uint8_t data[512] = { 0 };
	memset(data, 0, 512);

	// 将配置参数转换成存储的结构体
	Ini_File_Config_Param_To_Hex_Config_Param(data);

	// 制作hex格式的文件
	if (Make_Hex_File(data, 1024)) {
		// 在命名edit中显示名字

		//printf("制作Hex文件成功。\n");
		return true;
	}

	printf("制作Hex文件失败。\n");

	return false;
}
void CSensorParamHexDlg::OnBnClickedMfcbtnGenerate()
{
	//printf("%s,%d\n", __FUNCTION__, __LINE__);
	// 刷新配置参数
	Get_Ini_Config_Param_From_UI();

	// 写ini配置文件
	Write_Ini_Config_File();

	// 生成hex文件
	CString result_show;
	if (Generating_Hex_File()) {
		result_show.Format("%s 文件制作成功", m_hexFile_path);
	}
	else {
		result_show.Format("%s 文件制作失败", m_hexFile_path);
	}

	m_sta_result_show_handle.SetWindowText(result_show);
}

bool CSensorParamHexDlg::Get_Ini_Config_Param_From_UI()
{
	// 项目类型
	m_cfg_p.m_project_type.m_type_index = m_projct_comb_handle.GetCurSel();

	// 产品类型
	m_cfg_p.m_product_type.m_type_index = m_prodcr_comb_handle.GetCurSel();

	// 4G模块
	m_cfg_p.m_4g.m_func_sw = m_4g_cfg.m_func_btn_sw;										// 按钮状态
	m_cfg_p.m_4g.m_producer_index = m_4g_cfg.m_prodcr_comb_handle.GetCurSel();				// combox下标

	// GPS模块
	m_cfg_p.m_gps.m_func_sw = m_gps_cfg.m_func_btn_sw;										// 按钮状态
	m_cfg_p.m_gps.m_producer_index = m_gps_cfg.m_prodcr_comb_handle.GetCurSel();			// combox下标

	// IMU模块
	m_cfg_p.m_imu.m_func_sw = m_imu_cfg.m_func_btn_sw;										// 按钮状态
	m_cfg_p.m_imu.m_producer_index = m_imu_cfg.m_prodcr_comb_handle.GetCurSel();			// combox下标

	// 双目相机模块
	m_cfg_p.m_dbl_cam.m_func_sw = m_dbl_cam_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_dbl_cam.m_producer_index = m_dbl_cam_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标

	// 单目相机模块
	m_cfg_p.m_sgl_cam.m_func_sw = m_sgl_cam_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_sgl_cam.m_producer_index = m_sgl_cam_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标

	// 行车记录仪模块
	m_cfg_p.m_car_dvr.m_func_sw = m_car_dvr_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_car_dvr.m_producer_index = m_car_dvr_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标

	// 刹车制动模块
	m_cfg_p.m_brk_act.m_func_sw = m_brk_act_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_brk_act.m_producer_index = m_brk_act_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标

	// 前方超声波雷达
	m_cfg_p.m_ultra_front.m_func_sw = m_ultra_front_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_ultra_front.m_producer_index = m_ultra_front_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标 使用中的厂家
	m_cfg_p.m_ultra_front.controler_id = m_ultra_front_cfg.m_ctrler_comb_handle.GetCurSel();	// combox下标 控制器
	m_cfg_p.m_ultra_front.m_sensor_num = 0;															// 雷达数量
	if (m_ultra_front_cfg.m_rdr1_btn_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_ultra_front_cfg.m_rdr2_btn_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_ultra_front_cfg.m_rdr3_btn_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_ultra_front_cfg.m_rdr4_btn_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_ultra_front_cfg.m_rdr5_btn_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	if (m_ultra_front_cfg.m_rdr6_btn_sw) m_cfg_p.m_ultra_front.m_sensor_num++;
	//printf("Ultra_Front sensor_num:%d\n", m_cfg_p.m_ultra_front.m_sensor_num);

	m_cfg_p.m_ultra_front.m_rdr1_sw = m_ultra_front_cfg.m_rdr1_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_front.m_rdr2_sw = m_ultra_front_cfg.m_rdr2_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_front.m_rdr3_sw = m_ultra_front_cfg.m_rdr3_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_front.m_rdr4_sw = m_ultra_front_cfg.m_rdr4_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_front.m_rdr5_sw = m_ultra_front_cfg.m_rdr5_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_front.m_rdr6_sw = m_ultra_front_cfg.m_rdr6_btn_sw;	// 按钮状态

	// 后方超声波雷达
	m_cfg_p.m_ultra_end.m_func_sw = m_ultra_end_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_ultra_end.m_producer_index = m_ultra_end_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标
	m_cfg_p.m_ultra_end.controler_id = m_ultra_end_cfg.m_ctrler_comb_handle.GetCurSel();	// combox下标
	m_cfg_p.m_ultra_end.m_sensor_num = 0;														// 雷达数量
	if (m_ultra_end_cfg.m_rdr1_btn_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_ultra_end_cfg.m_rdr2_btn_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_ultra_end_cfg.m_rdr3_btn_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_ultra_end_cfg.m_rdr4_btn_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_ultra_end_cfg.m_rdr5_btn_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	if (m_ultra_end_cfg.m_rdr6_btn_sw) m_cfg_p.m_ultra_end.m_sensor_num++;
	//printf("Ultra_End sensor_num:%d\n", m_cfg_p.m_ultra_end.m_sensor_num);

	m_cfg_p.m_ultra_end.m_rdr1_sw = m_ultra_end_cfg.m_rdr1_btn_sw;		// 按钮状态
	m_cfg_p.m_ultra_end.m_rdr2_sw = m_ultra_end_cfg.m_rdr2_btn_sw;		// 按钮状态
	m_cfg_p.m_ultra_end.m_rdr3_sw = m_ultra_end_cfg.m_rdr3_btn_sw;		// 按钮状态
	m_cfg_p.m_ultra_end.m_rdr4_sw = m_ultra_end_cfg.m_rdr4_btn_sw;		// 按钮状态
	m_cfg_p.m_ultra_end.m_rdr5_sw = m_ultra_end_cfg.m_rdr5_btn_sw;		// 按钮状态
	m_cfg_p.m_ultra_end.m_rdr6_sw = m_ultra_end_cfg.m_rdr6_btn_sw;		// 按钮状态

	// 左侧超声波雷达
	m_cfg_p.m_ultra_left.m_func_sw = m_ultra_left_cfg.m_func_btn_sw;							// 按钮状态
	m_cfg_p.m_ultra_left.m_producer_index = m_ultra_left_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标
	m_cfg_p.m_ultra_left.controler_id = m_ultra_left_cfg.m_ctrler_comb_handle.GetCurSel();	// combox下标
	m_cfg_p.m_ultra_left.m_sensor_num = 0;														// 雷达数量
	if (m_ultra_left_cfg.m_rdr1_btn_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_ultra_left_cfg.m_rdr2_btn_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_ultra_left_cfg.m_rdr3_btn_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_ultra_left_cfg.m_rdr4_btn_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_ultra_left_cfg.m_rdr5_btn_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	if (m_ultra_left_cfg.m_rdr6_btn_sw) m_cfg_p.m_ultra_left.m_sensor_num++;
	//printf("Ultra_Left sensor_num:%d\n", m_cfg_p.m_ultra_left.m_sensor_num);

	m_cfg_p.m_ultra_left.m_rdr1_sw = m_ultra_left_cfg.m_rdr1_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_left.m_rdr2_sw = m_ultra_left_cfg.m_rdr2_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_left.m_rdr3_sw = m_ultra_left_cfg.m_rdr3_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_left.m_rdr4_sw = m_ultra_left_cfg.m_rdr4_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_left.m_rdr5_sw = m_ultra_left_cfg.m_rdr5_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_left.m_rdr6_sw = m_ultra_left_cfg.m_rdr6_btn_sw;	// 按钮状态

	// 右侧超声波雷达
	m_cfg_p.m_ultra_right.m_func_sw = m_ultra_right_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_ultra_right.m_producer_index = m_ultra_right_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标
	m_cfg_p.m_ultra_right.controler_id = m_ultra_right_cfg.m_ctrler_comb_handle.GetCurSel();	// combox下标
	m_cfg_p.m_ultra_right.m_sensor_num = 0;															// 雷达数量
	if (m_ultra_right_cfg.m_rdr1_btn_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_ultra_right_cfg.m_rdr2_btn_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_ultra_right_cfg.m_rdr3_btn_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_ultra_right_cfg.m_rdr4_btn_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_ultra_right_cfg.m_rdr5_btn_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	if (m_ultra_right_cfg.m_rdr6_btn_sw) m_cfg_p.m_ultra_right.m_sensor_num++;
	//printf("Ultra_Right sensor_num:%d\n", m_cfg_p.m_ultra_right.m_sensor_num);

	m_cfg_p.m_ultra_right.m_rdr1_sw = m_ultra_right_cfg.m_rdr1_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_right.m_rdr2_sw = m_ultra_right_cfg.m_rdr2_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_right.m_rdr3_sw = m_ultra_right_cfg.m_rdr3_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_right.m_rdr4_sw = m_ultra_right_cfg.m_rdr4_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_right.m_rdr5_sw = m_ultra_right_cfg.m_rdr5_btn_sw;	// 按钮状态
	m_cfg_p.m_ultra_right.m_rdr6_sw = m_ultra_right_cfg.m_rdr6_btn_sw;	// 按钮状态

	// 前方毫米波雷达
	m_cfg_p.m_mmw_front.m_func_sw = m_mmw_front_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_mmw_front.m_producer_index = m_mmw_front_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标
	m_cfg_p.m_mmw_front.m_rdr1_sw = m_mmw_front_cfg.m_rdr1_btn_sw;								// 按钮状态
	m_cfg_p.m_mmw_front.m_sensor_num = 0;														// 雷达数量
	if (m_mmw_front_cfg.m_rdr1_btn_sw) m_cfg_p.m_mmw_front.m_sensor_num++;
	//printf("MMW_Front sensor_num:%d\n", m_cfg_p.m_mmw_front.m_sensor_num);

	// 后方毫米波雷达
	m_cfg_p.m_mmw_end.m_func_sw = m_mmw_end_cfg.m_func_btn_sw;									// 按钮状态
	m_cfg_p.m_mmw_end.m_producer_index = m_mmw_end_cfg.m_prodcr_comb_handle.GetCurSel();		// combox下标
	m_cfg_p.m_mmw_end.m_rdr1_sw = m_mmw_end_cfg.m_rdr1_btn_sw;									// 按钮状态
	m_cfg_p.m_mmw_end.m_sensor_num = 0;														// 雷达数量
	if (m_mmw_end_cfg.m_rdr1_btn_sw) m_cfg_p.m_mmw_end.m_sensor_num++;
	//printf("MMW_end sensor_num:%d\n", m_cfg_p.m_mmw_end.m_sensor_num);

	// 左侧毫米波雷达
	m_cfg_p.m_mmw_left.m_func_sw = m_mmw_left_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_mmw_left.m_producer_index = m_mmw_left_cfg.m_prodcr_comb_handle.GetCurSel();		// combox下标
	m_cfg_p.m_mmw_left.m_rdr1_sw = m_mmw_left_cfg.m_rdr1_btn_sw;								// 按钮状态
	m_cfg_p.m_mmw_left.m_rdr2_sw = m_mmw_left_cfg.m_rdr2_btn_sw;								// 按钮状态
	m_cfg_p.m_mmw_left.m_sensor_num = 0;														// 雷达数量
	if (m_mmw_left_cfg.m_rdr1_btn_sw) m_cfg_p.m_mmw_left.m_sensor_num++;
	if (m_mmw_left_cfg.m_rdr2_btn_sw) m_cfg_p.m_mmw_left.m_sensor_num++;
	//printf("MMW_Left sensor_num:%d\n", m_cfg_p.m_mmw_left.m_sensor_num);

	// 右侧毫米波雷达
	m_cfg_p.m_mmw_right.m_func_sw = m_mmw_right_cfg.m_func_btn_sw;								// 按钮状态
	m_cfg_p.m_mmw_right.m_producer_index = m_mmw_right_cfg.m_prodcr_comb_handle.GetCurSel();	// combox下标
	m_cfg_p.m_mmw_right.m_rdr1_sw = m_mmw_right_cfg.m_rdr1_btn_sw;								// 按钮状态
	m_cfg_p.m_mmw_right.m_rdr2_sw = m_mmw_right_cfg.m_rdr2_btn_sw;								// 按钮状态
	m_cfg_p.m_mmw_right.m_sensor_num = 0;														// 雷达数量
	if (m_mmw_right_cfg.m_rdr1_btn_sw) m_cfg_p.m_mmw_right.m_sensor_num++;
	if (m_mmw_right_cfg.m_rdr2_btn_sw) m_cfg_p.m_mmw_right.m_sensor_num++;
	//printf("MMW_Right sensor_num:%d\n", m_cfg_p.m_mmw_right.m_sensor_num);

	return false;
}

BOOL CSensorParamHexDlg::File_Is_Exist(LPCTSTR lpszFile)
{
	HANDLE hFile = CreateFile(lpszFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	CloseHandle(hFile);
	return hFile != INVALID_HANDLE_VALUE;
}
