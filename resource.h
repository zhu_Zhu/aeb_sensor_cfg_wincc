﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 SensorParamHex.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SENSOR_PARAM_HEX_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDD_DLG_ULTRAL_DIS              130
#define IDD_DLG_WWM_DIS                 132
#define IDC_COMB_4G_Maker               1001
#define IDC_BTN_4G_Start                1002
#define IDC_BTN_GPS_Start               1003
#define IDC_BTN_Ultra_Front_Start       1004
#define IDC_BTN_Ultra_End_Start         1005
#define IDC_BTN_Ultra_Left_Start        1006
#define IDC_BTN_Ultra_Right_Start       1007
#define IDC_BTN_Ultra_Front_1           1008
#define IDC_BTN_Ultra_Front_2           1009
#define IDC_BTN_Ultra_Front_3           1010
#define IDC_BTN_Ultra_Front_4           1011
#define IDC_BTN_Ultra_Front_5           1012
#define IDC_BTN_Ultra_Front_6           1013
#define IDC_BTN_Ultra_End_1             1014
#define IDC_BTN_Ultra_End_2             1015
#define IDC_BTN_Ultra_End_3             1016
#define IDC_BTN_Ultra_End_4             1017
#define IDC_BTN_Ultra_End_5             1018
#define IDC_BTN_Ultra_End_6             1019
#define IDC_BTN_Ultra_Left_1            1020
#define IDC_BTN_Ultra_Left_2            1021
#define IDC_BTN_Ultra_Left_3            1022
#define IDC_BTN_Ultra_Left_4            1023
#define IDC_BTN_Ultra_Left_5            1024
#define IDC_BTN_Ultra_Left_6            1025
#define IDC_BTN_Ultra_Right_1           1026
#define IDC_BTN_Ultra_Right_2           1027
#define IDC_BTN_Ultra_Right_3           1028
#define IDC_BTN_Ultra_Right_4           1029
#define IDC_BTN_Ultra_Right_5           1030
#define IDC_BTN_Ultra_Right_6           1031
#define IDC_BTN_MMW_Front_Start         1032
#define IDC_BTN_MMW_End_Start           1033
#define IDC_BTN_MMW_Left_Start          1034
#define IDC_BTN_MMW_Right_Start         1035
#define IDC_COMB_GPS_Maker              1036
#define IDC_COMB_Dbl_Camera_Maker       1037
#define IDC_COMB_Sgl_Camera_Maker       1038
#define IDC_BTN_Dbl_Camera_Start        1039
#define IDC_BTN_Sgl_Camera_Start        1040
#define IDC_COMB_Ultra_Front_Maker      1041
#define IDC_BTN_MMW_Front_1             1042
#define IDC_BTN_MMW_End_1               1043
#define IDC_BTN_MMW_Left_1              1044
#define IDC_BTN_MMW_Left_2              1045
#define IDC_BTN_MMW_Right_1             1046
#define IDC_BTN_MMW_Right_2             1047
#define IDC_COMB_Ultra_End_Maker        1048
#define IDC_COMB_Ultra_Left_Maker       1049
#define IDC_COMB_Ultra_Right_Maker      1050
#define IDC_COMB_IMU_Maker              1051
#define IDC_BTN_IMU_Start               1052
#define IDC_COMB_Car_DVR_Maker          1053
#define IDC_BTN_Car_DVR_Start           1054
#define IDC_COMB_BRK_Act_Maker          1055
#define IDC_BTN_BRK_Act_Start           1056
#define IDC_COMB_Project_Maker          1057
#define IDC_COMB_Product_Maker          1058
#define IDC_COMB_MMW_Front_Maker        1059
#define IDC_COMB_MMW_End_Maker          1062
#define IDC_COMB_MMW_Left_Maker         1063
#define IDC_COMB_MMW_Right_Maker        1064
#define IDC_GRP_Ultra                   1065
#define IDC_GRP_MMW                     1066
#define IDC_STA_ULTRA_PIC               1067
#define IDC_STA_MMW_PIC                 1068
#define IDC_COMB_MMW_Front_Maker2       1072
#define IDC_MBTN_ULTRA_DIS              1075
#define IDC_MBTN_MMW_DIS                1076
#define IDC_COMB_Ultra_Front_Controler  1077
#define IDC_COMB_Ultra_End_Controler    1078
#define IDC_COMB_Ultra_Left_Controler   1079
#define IDC_COMB_Ultra_Right_Controler  1080
#define IDC_MBTN_GENERATE               1081
#define IDC_EDT_HEX_Name                1082
#define IDC_STA_Result_Show             1083
#define IDC_STA_4G                      1084
#define IDC_STA_GPS                     1085
#define IDC_STA_IMU                     1086
#define IDC_STA_Project                 1087
#define IDC_STA_Product                 1088
#define IDC_STA_Dbl_Cam                 1089
#define IDC_STA_Sgl_Cam                 1090
#define IDC_STA_Car_Dvr                 1091
#define IDC_STA_Brk_Act                 1092
#define IDC_STA_Ultra_Front             1093
#define IDC_STA_Ultra_End               1094
#define IDC_STA_Ultra_Left              1095
#define IDC_STA_Ultra_Right             1096
#define IDC_STA_MMW_Front               1097
#define IDC_STA_MMW_End                 1098
#define IDC_STA_MMW_Left                1099
#define IDC_STA_MMW_Right               1100
#define IDC_STA_Hex_Msg                 1111

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1112
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
